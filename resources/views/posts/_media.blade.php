@if($post->media_type == "youtube" || $post->media_type == "vimeo")
    <div class="videoWrapper">
        <iframe id="youtube-iframe"
                src="{{ $post->video_url }}"
                frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
        </iframe>
    </div>
@elseif($post->media_type == "image")
    @if(count($post->all_images) > 1)
        <div class="scroll">
            @foreach($post->all_images as $index => $image)
                <div data-aspect-ratio="custom" class="img-container">
                    <lazyload-img :imgsrc="'{{ $image }}'" :uniquename="'{{ "post_" . $post->id . "_" . $index }}'"
                                  :title="'{{ $post->title }}'"></lazyload-img>
                </div>
            @endforeach
        </div>
    @else
        <div data-aspect-ratio="custom" class="img-container">
            <lazyload-img :imgsrc="'{{ $post->large_image }}'" :uniquename="'{{ "post_" . $post->id }}'"
                          :title="'{{ $post->title }}'"></lazyload-img>
        </div>
    @endif
@else
    <img class="img-responsive" src="/images/image_not_available.png" alt="">
@endif