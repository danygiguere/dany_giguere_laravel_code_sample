@extends('layouts.app')
@section('title', 'Home')
@section('bodyId', 'home')
@section('content')
    @component('templates.main_with_sidebar')
        {{--***** <div class="col-xs-12 col-lg-8 main-content"> ***** --}}
        <div id="search-page">
            <search-page :query_prop="'{{ $query }}'" :page_prop="'{{ $page }}'"></search-page>
        </div>
        <div class="row">
            <div class="col-xs-12 hidden-lg">
                <adsense
                        ad-client="ca-pub-3937376096447808"
                        ad-slot="3112785579"
                        ad-style="display: block"
                        ad-format="auto">
                </adsense>
            </div>
            <div class="col-xs-12 hidden-lg">
                <adsense
                        ad-client="ca-pub-3937376096447808"
                        ad-slot="3112785579"
                        ad-style="display: block"
                        ad-format="auto">
                </adsense>
            </div>
        </div>
        {{--***** </div> ***** --}}
    @endcomponent
@endsection

