@extends('layouts.app')
@section('title', 'Post')
@section('bodyId', 'post')
@section('content')
    @component('templates.main_with_sidebar')
        {{--***** <div class="col-xs-12 col-lg-8 main-content"> ***** --}}
        <div class="post">
            <div class="row padding-bottom">
                <div class="col-xs-12">
                    <img class="padding-bottom img-responsive hidden-md hidden-lg" src="/images/adv-banner.jpg" alt="">
                </div>
                <div class="col-xs-12 padding-bottom-md">
                    <h1 class="post-title">{{ $post->title }}</h1>
                    <p class="info">By
                        <span><a class="author" href="{{ route('users.show', ['user' => $post->user->url]) }}">{{ $post->user->public_name }}</a></span>
                        <span class="date">&middot; {{ $post->publication_date }}</span>
                        <span class="views margin-right" title="Total views"> <i class="fa fa-eye"></i> {{ $totalViews }}</span>
                        <span class="fb-like margin-left" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></span>
                    </p>
                    <div class="sharethis-inline-share-buttons padding-vt"></div>
                    @include('posts._media', ['post' => $post])
                    <div class="article-text">
                        <p>{!! $post->text !!}</p>
                    </div>
                    <div class="right">
                        @auth
                            @if(Auth::user()->id == $post->user->id)
                                <a class="button" href="/posts/{{ $post->id }}/edit">Edit</a>
                            @endif
                        @endauth
                    </div>
                </div>
                <div class="col-xs-12 hidden-lg">
                    <adsense
                            ad-client="ca-pub-3937376096447808"
                            ad-slot="3112785579"
                            ad-style="display: block"
                            ad-format="auto">
                    </adsense>
                </div>
            </div>
        </div>
        <div class="header">
            <p class="header-title">Comments</p>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5" data-width="100%"></div>
            </div>
            <div class="col-xs-12 hidden-lg">
                <adsense
                        ad-client="ca-pub-3937376096447808"
                        ad-slot="3112785579"
                        ad-style="display: block"
                        ad-format="auto">
                </adsense>
            </div>
            <div class="col-xs-12">
                @include('users._author_block', ['user' => $post->user, 'fullblock' => false])
            </div>
        </div>
        <div class="category">
            <div class="header">
                <p class="header-title">By this author</p>
            </div>
            <div class="row padding-bottom">
                @foreach($postsByThisAuthor as $post)
                    <div class="col-xs-12 col-sm-6 col-md-4 padding-bottom-md">
                        @include('cached._post_excerpt', ['post' => $post])
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6 hidden-lg">
                <adsense
                        ad-client="ca-pub-3937376096447808"
                        ad-slot="3112785579"
                        ad-style="display: block"
                        ad-format="auto">
                </adsense>
            </div>
            <div class="col-xs-12 col-md-6 hidden-lg">
                <adsense
                        ad-client="ca-pub-3937376096447808"
                        ad-slot="3112785579"
                        ad-style="display: block"
                        ad-format="auto">
                </adsense>
            </div>
        </div>
        {{--***** </div> ***** --}}
    @endcomponent
@endsection

@section('javascripts')
    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a7b8af408e4bc00136bbbf1&product=inline-share-buttons"></script>
@endsection