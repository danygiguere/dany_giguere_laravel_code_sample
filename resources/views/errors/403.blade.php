@extends('layouts.app')
@section('title', '403')
@section('bodyId', 'four-o-three')
@section('content')
    @component('templates.main_without_sidebar')
        {{--***** <div class="col-xs-12 main-content"> ***** --}}
        <div class="padding-top-for-mobile">
            <h2 class="bold">Oh ohhhhh !</h2>
            <h4 class="gray">Sorry but you don't have authorization to view this page !</h4>
            <div class="padding-top">
                <a class="button margin-right" href="/">Go to home page</a><a class="button" href="{{ route('searchPage') }}">Go to search page</a>
            </div>
        </div>
        {{--***** <div> ***** --}}
    @endcomponent
@endsection

