<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="bbw8JgyqhY79XC9Ncp4pbuzIwa6d3Czm0ZKw4orV">

    <title>News</title>

    <!-- Styles -->
    <link rel="stylesheet" href="/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="http://news.test/css/app.css" rel="stylesheet">
    <script src="/js/modernizr-custom.js?id=facf1a74631a73251733"></script>
    <script>
        window.Laravel = {"csrfToken":"bbw8JgyqhY79XC9Ncp4pbuzIwa6d3Czm0ZKw4orV"};
    </script>
</head>
<body class="full-height">
<div id="fb-root"></div>
<script>
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div id="app">
    <div class="top-bar dark hidden-xs hidden-sm">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 center-vt">

                </div>
                <div class="col-xs-12 col-md-4">
                    <p>
                    <span class="right inline-block">
                        <a target=_blank href="#" title=Facebook>
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a target=_blank href="#" title=Twitter>
                            <i class="fa fa-google-plus"></i>
                        </a>
                        <a target=_blank href="#" title=Vimeo>
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a target=_blank href="#" title=VKontakte>
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a target=_blank href="#" title=Youtube>
                            <i class="fa fa-tumblr"></i>
                        </a>
                    </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <header>
        <nav class="hidden-md hidden-lg">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <a href="/"><img class="logo" src="/images/logo-mobile.gif" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container logo-container hidden-xs hidden-sm">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <a href="/"><img class="padding-top img-responsive" src="/images/logo-header@2x.gif" alt=""></a>
                </div>
                <div class="col-xs-12 col-md-8">
                    <img class="padding-vt img-responsive" src="/images/adv-banner.jpg" alt="">
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 main-content">
                    <div class="padding-top-for-mobile">
                        <h2 class="bold">Ooops... Our site is down for maintenance!</h2>
                        <h4 class="gray">Please come back soon !</h4>

                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="dark">
        <div class="color-overlay"></div>
        <div class="container">
            <div class="row center-hz">
                <div class="col-xs-10">
                    <div class="hr"></div>
                </div>
            </div>
            <div class="row text-center-sm">
                <div class="col-xs-12 col-md-3">
                    <img class="footer-logo img-responsive" src="/images/logo-footer.png" alt="">
                </div>
                <div class="col-xs-12 col-md-5">
                    <h4>About us</h4>
                    <div class="padding-vt">
                        <p>Newspaper Theme is your news, entertainment, music fashion website. We provide you with
                            the latest breaking news and videos straight from the entertainment industry.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <h4>Follow us</h4>
                    <div class="padding-vt">
                        <a class="social-icon-wrap" target=_blank href="#" title=Facebook>
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a class="social-icon-wrap" target=_blank href="#" title=Twitter>
                            <i class="fa fa-google-plus"></i>
                        </a>
                        <a class="social-icon-wrap" target=_blank href="#" title=Vimeo>
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a class="social-icon-wrap" target=_blank href="#" title=VKontakte>
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a class="social-icon-wrap" target=_blank href="#" title=Youtube>
                            <i class="fa fa-tumblr"></i>
                        </a>
                        <a class="social-icon-wrap" target=_blank href="#" title=Youtube>
                            <i class="fa fa-envelope"></i>
                        </a>
                        <div>
                            <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <section class="subfooter dark text-center-sm">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <p>© Copyright 2018 - Newspaper</p>
                </div>
                <div class="col-xs-12 col-md-8 links">
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Scripts -->
<script src="http://news.test/js/app.js"></script>
</body>
</html>
