@extends('layouts.app')
@section('title', '404')
@section('bodyId', 'four-o-four')
@section('content')
    @component('templates.main_without_sidebar')
        {{--***** <div class="col-xs-12 main-content"> ***** --}}
            <div class="padding-top-for-mobile">
                <h2 class="bold">Ooops... Error 404 !</h2>
                <h4 class="gray">Sorry but the page you're looking for doesn't seem to exist !</h4>
                <div class="padding-top">
                    <a class="button margin-right" href="/">Go to home page</a><a class="button" href="{{ route('searchPage') }}">Go to search page</a>
                </div>
            </div>
        {{--***** <div> ***** --}}
    @endcomponent
@endsection

