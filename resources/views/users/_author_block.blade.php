<div class="media author-block bordered margin-bottom ">
    <div class="media-left">
        <a href="{{ route('users.show', ['user' => $user->url]) }}">
            <img class="img-responsive" src="{{ $user->image_full_url }}" alt="">
        </a>
    </div>
    <div class="media-content">
        <h5><a href="{{ route('users.show', ['user' => $user->url]) }}">{{ $user->public_name }}</a></h5>
        @if($user->title)
            <p class="title margin-bottom">{{ $user->title }}</p>
        @endif
        @if($user->description)
            <p class="{{ $fullblock == false ? 'margin-bottom' : '' }}">
                @if($fullblock == false)
                    {{ $user->descriptionExcerpt }} @if(strlen($user->description) > 100)<a href="{{ route('users.show', ['user' => $user->url]) }}" class="view-more">view more</a>@endif
                @else
                    {{ $user->description }}
                @endif
            </p>
        @endif
        @if($fullblock == true)
            <div class="public_email">
                @if($user->public_email)
                    <p class="link"><a href="">{{ $user->public_email }}</a></p>
                @endif
                @if($user->public_email_2)
                    <p class="link"><a href="">{{ $user->public_email_2 }}</a></p>
                @endif
            </div>
            <div class="website">
                @if($user->website)
                    <p class="link"><a href="">{{ $user->website }}</a></p>
                @endif
                @if($user->website_2)
                    <p class="link"><a href="">{{ $user->website_2 }}</a></p>
                @endif
                @if($user->website_3)
                    <p class="link"><a href="">{{ $user->website_3 }}</a></p>
                @endif
            </div>
        @endif
        <p class="social-icons">
            @if($user->facebook)
                <a target=_blank href="{{ $user->facebook }}" title=Facebook>
                    <i class="fa fa-facebook"></i>
                </a>
            @endif
            @if($user->twitter)
                <a target=_blank href="{{ $user->twitter }}" title=Twitter>
                    <i class="fa fa-twitter"></i>
                </a>
            @endif
            @if($user->google_plus)
                <a target=_blank href="#" title=Google+>
                    <i class="fa fa-google-plus"></i>
                </a>
            @endif
            @if($user->youtube)
                <a target=_blank href="#" title=Youtube>
                    <i class="fa fa-youtube"></i>
                </a>
            @endif
            @if($user->vimeo)
                <a target=_blank href="#" title=Vimeo>
                    <i class="fa fa-vimeo"></i>
                </a>
            @endif
            @if($user->instagram)
                <a target=_blank href="#" title=Instagram>
                    <i class="fa fa-instagram"></i>
                </a>
            @endif
        </p>
    </div>
</div>