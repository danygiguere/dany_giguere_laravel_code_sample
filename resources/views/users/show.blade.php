@extends('layouts.app')
@section('title', 'User')
@section('bodyId', 'user')
@section('content')
    @component('templates.main_with_sidebar')
        {{--***** <div class="col-xs-12 col-lg-8 main-content"> ***** --}}
        <div class="row">
            <div class="col-xs-12">
                <img class="padding-bottom img-responsive hidden-md hidden-lg" src="/images/adv-banner.jpg" alt="">
            </div>
        </div>
        <h4 class="page-title">{{ $user->public_name }}</h4>
        @include('users._author_block', ['user' => $user, 'fullblock' => true])
        <div class="category">
            <div class="header">
                <p class="header-title">Posts by this author</p>
            </div>
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-xs-12 col-sm-6 col-md-4 padding-bottom-md">
                        <div data-aspect-ratio="custom" class="img-container">
                            <a href="{{ route('posts.show', ['category' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]) }}">
                                <img class="img-responsive" src="{{ $post->mediumImage }}" alt="">
                            </a>
                        </div>
                        <a href="{{ route('posts.show', ['category' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]) }}"><h4>{{ $post->title }}</h4></a>
                        <p class="info">
                            <span><a class="author" href="{{ route('users.show', ['user' => $user->url]) }}">{{ $user->public_name }}</a></span>
                            <span class="date">&middot; {{ $post->publication_date }}</span>
                        </p>
                    </div>
                @endforeach
                <div class="col-xs-12">
                    <div class="text-center">
                        {!! $posts->links() !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6 hidden-lg">
                    <adsense
                            ad-client="ca-pub-3937376096447808"
                            ad-slot="3112785579"
                            ad-style="display: block"
                            ad-format="auto">
                    </adsense>
                </div>
                <div class="col-xs-12 col-md-6 hidden-lg">
                    <adsense
                            ad-client="ca-pub-3937376096447808"
                            ad-slot="3112785579"
                            ad-style="display: block"
                            ad-format="auto">
                    </adsense>
                </div>
            </div>
        </div>
        {{--***** </div> ***** --}}
    @endcomponent
@endsection