@extends('layouts.app')
@section('title', 'Contact')
@section('bodyId', 'contact')
@section('content')
    @component('templates.main_without_sidebar')
        <div id="user-management">
            <div class="floatk margin-bottom">
                <a class="button left" href="{{ route('dashboard.usersManagement') }}">Back to Dashboard</a>
            </div>
            @if($user->is_rejected)
                <div class="warning margin-bottom">
                    <p>This user was rejected</p>
                </div>
            @endif
            @if (session('approved'))
                <div class="success">
                    <p>{{ session('approved') }}</p>
                </div>
            @endif
            @if (session('reactivated'))
                <div class="success">
                    <p>{{ session('reactivated') }}</p>
                </div>
            @endif
            @if($user->is_suspended)
                <div class="warning margin-bottom">
                    <p>This user was suspended</p>
                </div>
            @endif
            @if($user->is_banned)
                <div class="warning margin-bottom">
                    <p>This user was banned</p>
                </div>
            @endif
            @if($user->is_applicant)
                <h4 class="page-title padding-bottom">New application</h4>
            @else
                <h4 class="page-title padding-bottom">Author's page</h4>
            @endif
            <h5>Applicant username: {{ $user->name }}</h5>
            <h5>Applicant public name: {{ $user->public_name }}</h5>
            <h5>Applicant's email: {{ $user->email }}</h5>
            @if($user->isApplicant)
                <h5>Application text: {{ $user->application_text }}</h5>
            @endif
            <h5>User status: {{ $user->status }}</h5>
            <h5>User role: {{ $user->role }}</h5>
            <h5>Sign up date: {{ $user->created_at }}</h5>
            <h5>Suspended count: {{ $user->suspended_count }}</h5>
            <h5>Post removed: {{ $user->posts_removed_count }}</h5>
            @if($user->isApplicant)
                <div class="margin-vt">
                    <a class="button" href="{{ route('users.approve', ['user' => $user->url]) }}">Approve</a>
                </div>
                <div class="hr"></div>
                <div class="margin-top">
                    <form action="{{ route('users.reject', ['user' => $user->url]) }}" method="POST">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <textarea name="message" id="" cols="30" rows="10"></textarea>
                        <button type="submit" class="button">Reject</button>
                    </form>
                </div>
            @elseif($user->is_author)
                <div class="margin-top">
                    @if(!$user->isBlocked)
                        <form action="{{ route('users.suspend', ['user' => $user->url]) }}">
                            @if(!$user->isSuspended)
                                <button type="submit" class="button margin-right">Suspend</button>
                            @else
                                <a class="button margin-top"
                                   href="{{ route('users.unsuspend', ['user' => $user->url]) }}">Unsuspend</a>
                            @endif
                        </form>
                    @endif
                    <div class="hr margin-vt">
                        @if(!$user->is_banned)
                            <a class="button margin-top" href="{{ route('users.ban', ['user' => $user]) }}">Ban</a>
                        @else
                            <a class="button margin-top" href="{{ route('users.unban', ['user' => $user]) }}">UnBan</a>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    @endcomponent
@endsection

