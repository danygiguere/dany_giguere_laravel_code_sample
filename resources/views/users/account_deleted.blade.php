@extends('layouts.app')

@section('content')
    @component('templates.main_without_sidebar')
        {{--***** <div class="col-xs-12 main-content"> ***** --}}
            <div class="row text-center padding-top-for-mobile">
                <div class="col-xs-12 title-section padding-bottom">
                    <h2>Account Deleted</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
                    <p>We're sorry to see you going. Your account will be deleted and in the next few minutes.
                        All your drafts, posts and profile will be deleted from our system and we'll send a confirmation email once it's done !!
                    </p>
                    <p>We wish you all the best in all your endeavours. !!</p>
                </div>
            </div>
        {{--***** <div> ***** --}}
    @endcomponent
@endsection
