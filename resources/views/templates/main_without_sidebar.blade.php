<main>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 main-content">
                {{ $slot }}
            </div>
        </div>
    </div>
</main>