<main>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-8 main-content">
                {{ $slot }}
            </div>
            <div class="col-xs-12 col-lg-4">
                @include('_sidebar')
            </div>
        </div>
    </div>
</main>