@extends('layouts.app')
@section('title', 'Contact')
@section('bodyId', 'contact')
@section('content')
    @component('templates.main_without_sidebar')
            {{--***** <div class="col-xs-12 main-content"> ***** --}}
            <div class="row">
                <div class="col-xs-12 col-md-offset-1 col-sm-10">
                    @include('_privacy')
                </div>
            </div>
            {{--***** </div> ***** --}}
    @endcomponent
@endsection

