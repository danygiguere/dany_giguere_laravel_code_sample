<section id="most-popular">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <img class="padding-bottom img-responsive hidden-md hidden-lg" src="/images/adv-banner.jpg" alt="">
            </div>
            <div class="col-xs-12 col-md-6 first-position">
                <div class="img-container" data-aspect-ratio="big-custom">
                    <a href="{{ route('posts.show', ['category' => $trendingPosts->first()->category->url, 'post_slug' => $trendingPosts->first()->slug, 'post' => $trendingPosts->first()->id]) }}">
                        <span class="color-overlay"></span>
                        <div class="text-overlay">
                            <div class="bottom-left">
                                <button class="x-small">{{ $trendingPosts->first()->category->name }}</button>
                                <h2>{{ $trendingPosts->first()->excerptTitle }}</h2>
                                <p class="info">
                                    <span class="author">{{ $trendingPosts->first()->user->public_name }}</span>
                                    <span class="date">&middot; {{ $trendingPosts->first()->publication_date }}</span>
                                </p>
                            </div>
                        </div>
                        <lazyload-img :imgsrc="'{{ $trendingPosts->first()->largeImage }}'" :uniquename="'{{ "post_" . $trendingPosts->first()->id }}'" :title="'{{ $trendingPosts->first()->title }}'"></lazyload-img>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="row">
                    @foreach($trendingPosts->slice(1, 4) as $post)
                        <div class="col-xs-6">
                            <div class="img-container" data-aspect-ratio="custom">
                                <a href="{{ route('posts.show', ['category' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]) }}">
                                    <span class="color-overlay"></span>
                                    <div class="text-overlay">
                                        <div class="bottom-left">
                                            <button class="x-small">{{ $post->category->name }}</button>
                                            <h3>{{ $post->excerptTitle }}</h3>
                                        </div>
                                    </div>
                                    <lazyload-img :imgsrc="'{{ $post->mediumImage }}'" :uniquename="'{{ "post_" . $post->id }}'"></lazyload-img>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>