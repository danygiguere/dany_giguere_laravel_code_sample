<div data-aspect-ratio="custom" class="img-container">
<a href="{{ route('posts.show', ['category_url' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]) }}">
<lazyload-img :imgsrc="'{{ $post->mediumImage }}'" :uniquename="'{{ "post_" . $post->id }}'" :title="'{{ $post->title }}'"></lazyload-img>
</a>
</div>
<h4><a href="{{ route('posts.show', ['category_url' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]) }}">{{ $post->excerptTitle }}</a></h4>
<p class="info">
<span><a class="author" href="{{ route('users.show', ['user' => $post->user->url]) }}">{{ $post->user->real_name }}</a></span>
<span class="date">&middot; {{ $post->publication_date }}</span>
</p>
<p><a class="excerpt" href="{{ route('posts.show', ['category_url' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]) }}">{{ $post->excerptText }}</a></p>