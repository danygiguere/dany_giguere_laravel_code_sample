<div class="col-xs-2">
    <div data-aspect-ratio="custom" class="img-container">
        <a href="{{ route('posts.show', ['category_url' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]) }}">
            <img data-loads-on-hover="{{ $post->mediumImage }}" alt="" class="img-responsive center-vt-absolute fadeIn">
        </a>
    </div>
    <p><a href="{{ route('posts.show', ['category_url' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]) }}">{{ $post->title }}</a></p>
</div>