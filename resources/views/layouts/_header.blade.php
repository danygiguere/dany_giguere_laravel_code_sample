<div class="top-bar dark hidden-xs hidden-sm">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8 center-vt">
                <p>
                    <span class="today">{{ today()->formatLocalized('%A, %B %d') }}</span>
                    @guest
                        <a class="margin-left {{ activeIf('register') }}" href="{{ route('register') }}">Become an author</a>
                    @endguest
                    <a class="margin-left {{ activeIf('search') }}" href="{{ route('searchPage') }}">Search</a>
                    @auth
                        <a class="margin-left {{ activeIf('dashboard*') }}" href="{{ route('dashboard') }}">Dashboard</a>
                        <a class="margin-left" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" class="hidden" action="{{ route('logout') }}" method="POST">
                    {{ csrf_field() }}
                </form>
                @endauth
                </p>
            </div>
            <div class="col-xs-12 col-md-4">
                <p>
                    <span class="right inline-block">
                        <a target=_blank href="#" title=Facebook>
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a target=_blank href="#" title=Twitter>
                            <i class="fa fa-google-plus"></i>
                        </a>
                        <a target=_blank href="#" title=Vimeo>
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a target=_blank href="#" title=VKontakte>
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a target=_blank href="#" title=Youtube>
                            <i class="fa fa-tumblr"></i>
                        </a>
                    </span>
                </p>
            </div>
        </div>
    </div>
</div>
<header>
    <nav class="hidden-md hidden-lg">
        <div class="mobile-menu-button">
            <div class="bars">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <a href="/"><img class="logo" src="/images/logo-mobile.gif" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-search-button">
            <p><i class="fa fa-search"></i></p>
        </div>
        <div class="mobile-menu">
            <div class="color-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 content">
                        <div class="close-mobile-menu">
                            <i class="fa fa-2x fa-close"></i>
                        </div>
                        <p class="social-icons">
                            <a target=_blank href="#" title=Facebook>
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a target=_blank href="#" title=Twitter>
                                <i class="fa fa-google-plus"></i>
                            </a>
                            <a target=_blank href="#" title=Vimeo>
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a target=_blank href="#" title=VKontakte>
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a target=_blank href="#" title=Youtube>
                                <i class="fa fa-tumblr"></i>
                            </a>
                        </p>
                    </div>
                    <div class="col-xs-12 content">
                        <ul>
                            <li class="{{ activeIf('/') }}"><h2><a class="category" href="/">Trending</a></h2></li>
                            @foreach($categories as $category)
                                <li class="{{ activeIf($category->url) }}"><h2><a class="category" href="{{ route('category', ['category' => $category->url]) }}">{{ $category->name }}</a></h2></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-search">
            <div class="color-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 content">
                        <div class="close-mobile-search">
                            <i class="fa fa-2x fa-close"></i>
                        </div>
                        <mobile-menu-search></mobile-menu-search>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="container logo-container hidden-xs hidden-sm">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <a href="/"><img class="padding-top img-responsive" src="/images/logo-header@2x.gif" alt=""></a>
            </div>
            <div class="col-xs-12 col-md-8">
                <img class="padding-vt img-responsive" src="/images/adv-banner.jpg" alt="">
            </div>
        </div>
    </div>
    <div class="menu-bar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="menu"><a href="/" class="menu-bar-logo"><img
                                    src="/images/logo-header@2x.gif" alt=""></a>
                        <ul class="scroll">
                            <li class="{{ activeIf('/') }}">
                                <a href="/">Trending</a>
                                <div class="categories-previews-container">
                                    <div class="container">
                                        <div class="category-previews-box">
                                            <div class="row">
                                                @foreach($trendingPosts as $post)
                                                    @include('cached._header_post', ['post' => $post])
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @foreach($trendingPostsForAllCategories as $trendingPostsForCategory)
                                <li class="{{ activeIf($trendingPostsForCategory[0]->category->url) }}">
                                    <a href="{{ route('category', ['category' => $trendingPostsForCategory[0]->category]) }}">{{ $trendingPostsForCategory[0]->category->name }}</a>
                                    <div class="categories-previews-container">
                                        <div class="container">
                                            <div class="category-previews-box">
                                                <div class="row">
                                                    @foreach($trendingPostsForCategory as $post)
                                                        @include('cached._header_post', ['post' => $post])
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <div id="menu-search">
                            <menu-search></menu-search>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>