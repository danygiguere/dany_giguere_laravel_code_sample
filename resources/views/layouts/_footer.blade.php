<footer class="dark">
    <div class="color-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <h4>Latest News</h4>
                <div class="padding-top">
                    @foreach($latestPosts as $post)
                        @include('cached._footer_post', ['post' => $post])
                    @endforeach
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <h4>Trending News</h4>
                <div class="padding-top">
                    @foreach($trendingPosts as $post)
                        @include('cached._footer_post', ['post' => $post])
                    @endforeach
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <h4>Suggestions</h4>
                <div class="padding-top">
                    @foreach($suggestionsPosts as $post)
                        @include('cached._footer_post', ['post' => $post])
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row center-hz">
            <div class="col-xs-10">
                <div class="hr"></div>
            </div>
        </div>
        <div class="row text-center-sm">
            <div class="col-xs-12 col-md-3">
                <img class="footer-logo img-responsive" src="/images/logo-footer.png" alt="">
            </div>
            <div class="col-xs-12 col-md-5">
                <h4>About us</h4>
                <div class="padding-vt">
                    <p>Newspaper Theme is your news, entertainment, music fashion website. We provide you with
                        the latest breaking news and videos straight from the entertainment industry.</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <h4>Follow us</h4>
                <div class="padding-vt">
                    <a class="social-icon-wrap" target=_blank href="#" title=Facebook>
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a class="social-icon-wrap" target=_blank href="#" title=Twitter>
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a class="social-icon-wrap" target=_blank href="#" title=Vimeo>
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a class="social-icon-wrap" target=_blank href="#" title=VKontakte>
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a class="social-icon-wrap" target=_blank href="#" title=Youtube>
                        <i class="fa fa-tumblr"></i>
                    </a>
                    <a class="social-icon-wrap" target=_blank href="#" title=Youtube>
                        <i class="fa fa-envelope"></i>
                    </a>
                    <div>
                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<section class="subfooter dark text-center-sm">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <p>© Copyright {{ \Carbon\Carbon::now()->year }} - Newspaper</p>
            </div>
            <div class="col-xs-12 col-md-8 links">
                <p>
                    <a class="{{ activeIf('privacy') }}" href="{{ route('privacy') }}">Privacy</a>
                    <a class="{{ activeIf('terms') }}" href="{{ route('terms') }}">Terms</a>
                    {{--<a class="margin-left {{ activeIf('advertisement') }}" href="">Advertisement</a>--}}
                    @auth
                        <a class="margin-left {{ activeIf('dashboard*') }}" href="{{ route('dashboard') }}">Dashboard</a>
                        <a class="margin-left {{ activeIf('guidelines') }}" href="{{ route('guidelines') }}">Guidelines</a>
                        <a class="margin-left" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    @else
                        <a class="margin-left {{ activeIf('register') }}" href="{{ route('register') }}">Become an author</a>
                        <a class="margin-left {{ activeIf('login') }}" href="{{ route('login') }}">Login</a>
                    @endauth
                </p>
            </div>
        </div>
    </div>
</section>