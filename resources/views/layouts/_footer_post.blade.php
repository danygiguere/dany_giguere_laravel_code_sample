<div class="media padding-bottom">
    <div class="media-left">
        <div data-aspect-ratio="custom" class="img-container">
            <a href="{{ route('posts.show', ['category' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]) }}">
                <lazyload-img :imgsrc="'{{ $post->smallImage }}'" :uniquename="'{{ "post_" . $post->id }}'"
                              :title="'{{ $post->title }}'"></lazyload-img>
            </a>
        </div>
    </div>
    <div class="media-content">
        <h5>
            <a href="{{ route('posts.show', ['category' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]) }}">{{ $post->excerptTitle }}</a>
        </h5>
        <p class="info">
            <span class="date">{{ $post->publication_date }}</span>
        </p>
    </div>
</div>
