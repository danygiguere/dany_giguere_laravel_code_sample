@extends('layouts.app')
@section('title', 'Home')
@section('bodyId', 'home')
@section('content')
    @include('cached._most_popular', ['trendingPosts' => $trendingPosts])
    @component('templates.main_with_sidebar')
         {{--***** <div class="col-xs-12 col-lg-8 main-content"> ***** --}}
        <div class="category">
            <div class="header">
                <p class="header-title">Latest</p>
            </div>
            <div class="row">
                @foreach($latestPostsForCategory as $post)
                    <div class="col-xs-12 col-sm-6 col-lg-4 padding-bottom-md">
                        @include('cached._post_excerpt', ['post' => $post])
                    </div>
                    @if($loop->iteration % 4 == 0)
                        <div class="col-xs-12 col-sm-6 col-lg-4 padding-bottom-md ad-like hidden-lg">
                            <adsense
                                    ad-client="ca-pub-3937376096447808"
                                    ad-slot="3112785579"
                                    ad-style="display: block"
                                    ad-format="auto">
                            </adsense>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="row padding-bottom">
                <div class="col-xs-12 col-md-6 hidden-lg">
                    <adsense
                            ad-client="ca-pub-3937376096447808"
                            ad-slot="3112785579"
                            ad-style="display: block"
                            ad-format="auto">
                    </adsense>
                </div>
                <div class="col-xs-12 col-md-6 hidden-lg">
                    <adsense
                            ad-client="ca-pub-3937376096447808"
                            ad-slot="3112785579"
                            ad-style="display: block"
                            ad-format="auto">
                    </adsense>
                </div>
            </div>
        </div>
         {{--***** </div> ***** --}}
    @endcomponent
@endsection

