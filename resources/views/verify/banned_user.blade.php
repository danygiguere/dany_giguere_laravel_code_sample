@extends("layouts.app")
@section("content")
    @component('templates.main_without_sidebar')
        <div id="verification" class="row padding-top-for-mobile">
            <div class="col-xs-12 col-md-8">
                <h4>Banned account</h4>
                <p>Sorry, it appears that you've been banned. We have sent you an email with more information.</p>
            </div>
        </div>
    @endcomponent
@endsection