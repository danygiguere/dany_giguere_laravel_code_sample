@extends("layouts.app")
@section("content")
    @component('templates.main_without_sidebar')
        <div id="verification" class="row padding-top-for-mobile">
            <div class="col-xs-12 col-md-8">
                <h4>You need to verify your email</h4>
                <p>You're verification token has expired or you are trying to login but you haven't verify your mail. Do you need a new verification link ? <a href="{{ route('verify.resendVerificationEmail', ['auth_token' => $auth_token]) }}">Send a new verification link</a></p>
            </div>
        </div>
    @endcomponent
@endsection