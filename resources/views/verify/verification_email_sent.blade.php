@extends('layouts.app')
@section('content')
    @component('templates.main_without_sidebar')
        <div id="verification" class="row padding-top-for-mobile">
            <div class="col-xs-12 col-md-8">
                <h4>Verification email sent</h4>
                <p>We have received your application and an email with a verification link was sent to you. Please go to
                    your mailbox and click on the email verification link so that we can review your
                    application as soon as possible.</p>
            </div>
        </div>
    @endcomponent
@endsection