@extends("layouts.app")
@section("content")
    @component('templates.main_without_sidebar')
        <div id="verification" class="row padding-top-for-mobile">
            <div class="col-xs-12 col-md-8">
                <h4>Email verified</h4>
                <p>Your Email was successfully verified. We should now be able to review your application very soon (usually within 72 hours). As soon as we're done we'll contact you with the email we have from you on file.</p>
            </div>
        </div>
    @endcomponent
@endsection