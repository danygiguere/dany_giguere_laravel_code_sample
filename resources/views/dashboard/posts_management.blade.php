@extends('layouts.app')
@section('bodyId', 'posts')
@section('content')
    @component('dashboard.template')
        <dashboard-posts-management></dashboard-posts-management>
    @endcomponent
@endsection
