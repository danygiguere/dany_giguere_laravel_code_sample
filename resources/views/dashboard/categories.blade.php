@extends('layouts.app')
@section('bodyId', 'index')
@section('content')
    @component('dashboard.template')
        <div id="tab-profile" class="tab-content bordered">
            <div class="row">
                <div class="col-xs-12 col-lg-6 padding-bottom-md">
                    <div class="scroll">
                        <table>
                            <tbody>
                            <tr>
                                <th colspan="2" class="text-center bold">Current Categories</th>
                            </tr>
                            <tr>
                                <th class="bold">Name</th>
                                <th class="bold">Url</th>
                            </tr>
                            @foreach($allCategories as $category)
                                <tr>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->url }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 padding-bottom-md">
                    <form id="" role="form" method="POST" action="{{ route('dashboard.storeCategories') }}">
                        {{ csrf_field() }}
                        <label>
                            Category Name:
                            <input type="text" name="name">
                        </label>
                        <label>
                            Category Url:
                            <input type="text" name="url">
                        </label>
                        <div class="right">
                            <button class="button submit">Create new category</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endcomponent
@endsection

@section('javascripts')

@endsection