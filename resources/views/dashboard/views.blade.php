@extends('layouts.app')
@section('bodyId', 'posts')
@section('content')
    @component('dashboard.template')
        <dashboard-views :views_prop="{{ $views }}"></dashboard-views>
    @endcomponent
@endsection
