@extends('layouts.app')
@section('bodyId', 'posts')
@section('content')
    @component('dashboard.template')
        <dashboard-profile :user_prop="{{ $user }}"></dashboard-profile>
    @endcomponent
@endsection
