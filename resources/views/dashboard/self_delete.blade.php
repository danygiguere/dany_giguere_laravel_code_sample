@extends('layouts.app')

@section('content')
    @component('templates.main_without_sidebar')
        {{--***** <div class="col-xs-12 main-content"> ***** --}}
        <div class="row padding-bottom">
            <div class="col-xs-12">
                <div class="float">
                    <a class="button left" href="{{ route('dashboard.posts') }}">Back to Dashboard</a>
                </div>
            </div>
        </div>
            <div class="row text-center padding-top-for-mobile">
                <div class="col-xs-12 title-section padding-bottom">
                    <h2>Delete my Account</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
                    <form method="POST" action="/users/self-delete">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <h4>Are you sure you want to permanently delete your account ?</h4>
                        <delete-my-account></delete-my-account>
                    </form>
                </div>
            </div>
        {{--***** <div> ***** --}}
    @endcomponent
@endsection
