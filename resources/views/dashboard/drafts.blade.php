@extends('layouts.app')
@section('bodyId', 'posts')
@section('content')
    @component('dashboard.template')
        <dashboard-drafts :user_prop="{{ Auth::user() }}"></dashboard-drafts>
    @endcomponent
@endsection
