@extends('layouts.app')
@section('bodyId', 'index')
@section('content')
    @component('dashboard.template')
        <div id="tab-profile" class="tab-content bordered">
            <div class="row">
                <div class="col-xs-12">
                    @if($user->is_applicant)
                        <div>
                            <h4 class="padding-top">Hello {{ $user->public_name }}</h4>
                            <p>Your account is under review</p>
                        </div>
                    @endif
                    @if($user->is_suspended)
                        <div>
                            <h4 class="padding-top">Hello {{ $user->public_name }}</h4>
                            <p>You'be been suspended for 72hours. You won't be able to publish before {{ $suspendedUntil }}</p>
                        </div>
                    @endif
                </div>
                <div class="col-xs-12 col-lg-6 margin-bottom">
                    <div class="scroll">
                        <table>
                            <tbody>
                            <tr>
                                <th colspan="2" class="text-center bold">Posts</th>
                            </tr>
                            <tr>
                                <th class="bold">Posts published today</th>
                                <td>{{ $numberOfPostsToday }}</td>
                            </tr>
                            <tr>
                                <th class="bold">Views for posts published today</th>
                                <td>{{ $viewsForPostsPublishedToday }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 margin-bottom">
                    <div class="scroll">
                        <table>
                            <tbody>
                            <tr>
                                <th colspan="2" class="text-center bold">Account</th>
                            </tr>
                            <tr>
                                <th class="bold">Role</th>
                                <td><span>{{ $user->role }}</span></td>
                            </tr>
                            <tr>
                                <th class="bold">Status</th>
                                <td><span>{{ $user->status }}</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="margin-top">
                        <div class="scroll">
                            <table>
                                <tbody>
                                <tr>
                                    <th colspan="2" class="text-center bold">Payment</th>
                                </tr>
                                <tr>
                                    <th class="bold">Date of last payment</th>
                                    <td><span>NA</span></td>
                                </tr>
                                <tr>
                                    <th class="bold">Payment</th>
                                    <td><span>0$</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if($user->is_superadmin)
                    <div class="col-xs-12">
                        <div class="margin-top">
                            <a class="button" target="_blank" href="/horizon">Visit horizon</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    @endcomponent
@endsection

@section('javascripts')

@endsection