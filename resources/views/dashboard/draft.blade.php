@extends('layouts.app')
@section('title', 'Post')
@section('bodyId', 'post')
@section('content')
    @component('templates.main_with_sidebar')
{{--<main>--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-xs-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 main-content">--}}
                <div class="post">
                    <div class="row padding-bottom">
                        <div class="col-xs-12">
                            <div class="float">
                                <a class="button left" href="{{ route('dashboard.posts') }}">Back to Dashboard</a>
                            </div>
                        </div>
                    </div>
                    <dashboard-draft :categories_prop="{{ $allCategories }}" :draft_prop="{{ $draft }}" :draft_images_prop="{{ $draftImages }}" :draft_youtube_video_prop="{{ $draftYoutubeVideo }}" :draft_vimeo_video_prop="{{ $draftVimeoVideo }}"></dashboard-draft>
                </div>
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</main>--}}
    @endcomponent
@endsection