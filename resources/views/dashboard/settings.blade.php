@extends('layouts.app')
@section('bodyId', 'posts')
@section('content')
    @component('dashboard.template')
        <dashboard-settings :user_prop="{{ $user }}"></dashboard-settings>
    @endcomponent
@endsection
