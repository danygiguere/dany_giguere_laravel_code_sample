<main id="dashboard">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 main-content">
                <div class="dashboard">
                    <div class="tab-block">
                        <div class="scroll">
                            <div class="tab"><a class="{{ activeIf('dashboard') }}" href="{{ route('dashboard') }}">Summary</a></div>
                            @if(!Auth::user()->isSelfDeactivated)
                            <div class="tab"><a class="{{ activeIf('dashboard/posts') }}" href="{{ route('dashboard.posts') }}">Posts</a></div>
                            @endif
                            <div class="tab"><a class="{{ activeIf('dashboard/views') }}" href="{{ route('dashboard.views') }}">Views</a></div>
                            @if(!Auth::user()->isSelfDeactivated)
                            <div class="tab"><a class="{{ activeIf('dashboard/profile') }}" href="{{ route('dashboard.profile') }}">Profile</a></div>
                            @endif
                            <div class="tab"><a class="{{ activeIf('dashboard/payments') }}" href="{{ route('dashboard.payments') }}">Payments</a></div>
                            @if(Auth::user()->is_superadmin or Auth::user()->is_admin)
                            <div class="tab"><a class="{{ activeIf('dashboard/users-management') }}" href="{{ route('dashboard.usersManagement') }}">Users management</a></div>
                            @endif
                            @if(Auth::user()->is_superadmin or Auth::user()->is_admin)
                                <div class="tab"><a class="{{ activeIf('dashboard/posts-management') }}" href="{{ route('dashboard.postsManagement') }}">Posts management</a></div>
                            @endif
                            @if(Auth::user()->is_superadmin)
                                <div class="tab"><a class="{{ activeIf('dashboard/categories') }}" href="{{ route('dashboard.categories') }}">Categories</a></div>
                            @endif
                            <div class="tab"><a class="{{ activeIf('dashboard/settings') }}" href="{{ route('dashboard.settings') }}">Settings</a></div>
                        </div>
                        <div class="tab-contents">
                            {{ $slot }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>