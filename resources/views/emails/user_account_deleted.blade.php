@component('mail::message')
# We're writing to you to inform you that your drafts, posts and profile have all been permanently deleted from our system.

We wish you all the best in all your endeavours.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
