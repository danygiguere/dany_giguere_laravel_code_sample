@component('mail::message')
# Hi {{ $user->public_name }}!

Your recent post {{ $post->title }} has been removed.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
