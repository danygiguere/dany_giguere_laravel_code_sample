@component('mail::message')
# Hi {{ $user->public_name }}!

We're writing to you to let you know that you've have been banned from News.

Thanks,<br>
{{ config('app.name') }}
@endcomponent