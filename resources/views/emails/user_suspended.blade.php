@component('mail::message')
# Hi {{ $user->public_name }}!

Your account has been suspended.

@component('mail::button', ['url' => url('/login')])
Review Guideline
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

