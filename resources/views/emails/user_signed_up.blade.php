@component('mail::message')
# Hi {{ $user->public_name }}!

Thank you for signing up ! Your email have successfully been verified and we will soon review your application.

@component('mail::button', ['url' => url('/login')])
Review your application
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
