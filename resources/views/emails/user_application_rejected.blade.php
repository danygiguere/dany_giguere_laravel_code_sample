@component('mail::message')
# Hi {{ $user->public_name }}!

We're writing to you to inform you that your application to News was rejected for the following reasons:

{{ $message }}

You can reply to this email if you want to reapply...

Thanks,<br>
{{ config('app.name') }}
@endcomponent
