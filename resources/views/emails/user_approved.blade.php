@component('mail::message')
# Hi {{ $user->public_name }}!

Welcome to News. You're application have been approved and you can now login and start posting on news

@component('mail::button', ['url' => url('/login')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
