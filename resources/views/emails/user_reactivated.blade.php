@component('mail::message')
# Hi {{ $user->public_name }}!

Welcome to News. You're account has been reactivated.

@component('mail::button', ['url' => url('/login')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
