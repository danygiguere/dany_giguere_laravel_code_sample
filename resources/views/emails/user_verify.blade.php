@component('mail::message')
# Hi {{ $user->public_name }}!

We have received your application. In order to review it, we'd like you to verify your e-mail by clicking the button below.

@component('mail::button', ['url' => url('verify-email/' . $user->verification_token)])
Verify your email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent