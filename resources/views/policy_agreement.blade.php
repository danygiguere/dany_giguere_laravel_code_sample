@extends('layouts.app')
@section('title', 'Contact')
@section('bodyId', 'contact')
@section('content')
    @component('templates.main_without_sidebar')
            {{--***** <div class="col-xs-12 main-content"> ***** --}}
            <div class="row">
                <div class="col-xs-12 col-md-offset-1 col-sm-10">
                    @if(Auth::user()->last_agreed_policy != App\PolicyAgreement::currentPolicyNumber())
                        <h5>New policy agreement</h5>

                        <p>You need to agree to the new Terms of Service and Privacy Policy.</p>

                        @include('_terms')
                        @include('_privacy')

                        <form method="GET" action="{{ route('agreeToPolicyAgreement') }}">
                            <button type="submit">Agree</button>
                        </form>
                    @else
                        <p>You have already agreed to the last policy ({{ App\PolicyAgreement::currentPolicyNumber() }})</p>
                    @endif
                </div>
            </div>
            {{--***** </div> ***** --}}
    @endcomponent
@endsection

