@php
    if (Cache::tags(["post_$post->id"])->has('header_post_'.$post->id)) {
        echo Cache::tags(["post_$post->id"])->get('header_post_'.$post->id);
    } else {
        Cache::tags(["post_$post->id"])->put('header_post_'.$post->id, view('layouts._header_post', ['post' => $post])->render(), config('app.redis_caching_time'));
        echo Cache::tags(["post_$post->id"])->get('header_post_'.$post->id);
    }
@endphp