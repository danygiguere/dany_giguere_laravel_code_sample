@php
    if (Cache::tags(["posts"])->has('most_popular_'.$trendingPosts->first()->category->url)) {
        echo Cache::tags(["posts"])->get('most_popular_'.$trendingPosts->first()->category->url);
    } else {
        Cache::tags(["posts"])->put('most_popular_'.$trendingPosts->first()->category->url, view('_most_popular', ['trendingPosts' => $trendingPosts])->render(), config('app.redis_caching_time_medium'));
        echo Cache::tags(["posts"])->get('most_popular_'.$trendingPosts->first()->category->url);
    }
@endphp