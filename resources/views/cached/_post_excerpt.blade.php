@php
    if (Cache::tags(["post_$post->id"])->has('post_excerpt_'.$post->id)) {
        echo Cache::tags(["post_$post->id"])->get('post_excerpt_'.$post->id);
    } else {
        Cache::tags(["post_$post->id"])->put('post_excerpt_'.$post->id, view('_post_excerpt', ['post' => $post])->render(), config('app.redis_caching_time'));
        echo Cache::tags(["post_$post->id"])->get('post_excerpt_'.$post->id);
    }
@endphp