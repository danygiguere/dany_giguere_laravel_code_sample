@php
    if (Cache::tags(["post_$post->id"])->has('footer_post_'.$post->id)) {
        echo Cache::tags(["post_$post->id"])->get('footer_post_'.$post->id);
    } else {
        Cache::tags(["post_$post->id"])->put('footer_post_'.$post->id, view('layouts._footer_post', ['post' => $post])->render(), config('app.redis_caching_time'));
        echo Cache::tags(["post_$post->id"])->get('footer_post_'.$post->id);
    }
@endphp

