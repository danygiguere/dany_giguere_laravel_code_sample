@extends('layouts.app')
@section('title', 'Home')
@section('bodyId', 'home')
@section('content')
    @include('cached._most_popular', ['trendingPosts' => $trendingPosts])
    @component('templates.main_with_sidebar')
        {{--***** <div class="col-xs-12 col-lg-8 main-content"> ***** --}}
        @foreach($trendingPostsForAllCategories as $trendingPostsForCategory)
            @if($loop->iteration <= 3)
                <div class="category">
                    <div class="header">
                        <p class="header-title">{{ $trendingPostsForCategory[0]->category->name }}</p>
                    </div>
                    <div class="row">
                        @foreach(array_slice($trendingPostsForCategory, 0, 4) as $post)
                            <div class="col-xs-12 col-sm-6 padding-bottom-md">
                                @include('cached._post_excerpt', ['post' => $post])
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-xs-12 hidden-lg">
                    <adsense
                            ad-client="ca-pub-3937376096447808"
                            ad-slot="3112785579"
                            ad-style="display: block"
                            ad-format="auto">
                    </adsense>
                </div>
            @endif
            @if($loop->iteration > 3)
                <div class="category">
                    <div class="header">
                        <p class="header-title">{{ $trendingPostsForCategory[0]->category->name }}</p>
                    </div>
                    <div class="row">
                        @foreach(array_slice($trendingPostsForCategory, 0, 3) as $post)
                            <div class="col-xs-12 col-sm-4 col-lg-4 padding-bottom-md">
                                @include('cached._post_excerpt', ['post' => $post])
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-xs-12 @if($loop->iteration % 2 == 0) hidden-sm hidden-md @endif hidden-lg">
                    <adsense
                            ad-client="ca-pub-3937376096447808"
                            ad-slot="3112785579"
                            ad-style="display: block"
                            ad-format="auto">
                    </adsense>
                </div>
            @endif
        @endforeach
        {{--***** </div> ***** --}}
    @endcomponent
@endsection

