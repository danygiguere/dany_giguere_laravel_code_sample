@extends('layouts.app')

@section('content')
    @component('templates.main_without_sidebar')
        {{--***** <div class="col-xs-12 main-content"> ***** --}}
        <div class="row text-center padding-top-for-mobile">
            <div class="col-xs-12 title-section padding-bottom">
                <h2>Reset Password</h2>
            </div>
        </div>
        <form method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" name="email" placeholder="Email Address" value="{{ $email or old('email') }}" autofocus>
                        @if ($errors->has('email'))
                            <span class="error">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" placeholder="Password" name="password">
                        @if ($errors->has('password'))
                            <span class="error">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input id="password-confirm" type="password" placeholder="Confirm Password" name="password_confirmation">

                        @if ($errors->has('password_confirmation'))
                            <span class="error">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit">
                            Reset Password
                        </button>
                    </div>
                </div>
            </div>
        </form>
        {{--***** <div> ***** --}}
    @endcomponent
@endsection
