@extends('layouts.app')

@section('content')
    @component('templates.main_without_sidebar')
        {{--***** <div class="col-xs-12 main-content"> ***** --}}
            <div class="row text-center padding-top-for-mobile">
                <div class="col-xs-12 title-section padding-bottom">
                    <h2>Reset Password</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
                    @if (session('status'))
                        <div class="success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
            <form method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" name="email" placeholder="Email Address" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="error">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit">
                                Send Password Reset Link
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        {{--***** <div> ***** --}}
    @endcomponent
@endsection
