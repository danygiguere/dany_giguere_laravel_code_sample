@extends('layouts.app')

@section('content')
    @component('templates.main_without_sidebar')
        {{--***** <div class="col-xs-12 main-content"> ***** --}}
            <div class="row text-center padding-top-for-mobile">
                <div class="col-xs-12 title-section padding-bottom">
                    <h2>Become an author</h2>
                    <p>Sign up with us and get paid to post articles. Some conditions apply *</p>
                </div>
            </div>
            <form id="sign-up-form" role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="padding-box">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {{--<label for="email">Email</label>--}}
                                <input type="text" name="email" id="email" placeholder="Email (for login)"
                                       value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                {{--<label for="password">Password</label>--}}
                                <input type="password" name="password" id="password" placeholder="Password" value="">
                                @if ($errors->has('password'))
                                    <span class="error">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                {{--<label for="password_confirmation">Confirm Password</label>--}}
                                <input type="password" name="password_confirmation" id="password_confirmation"
                                       placeholder="Confirm Password" value="">
                                @if ($errors->has('password_confirmation'))
                                    <span class="error">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                {{--<label for="name">Username</label>--}}
                                <input type="text" name="name" placeholder="Username"
                                       value="{{ old('name') }}" autofocus>
                                @if ($errors->has('name'))
                                    <span class="error">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('public_name') ? ' has-error' : '' }}">
                                {{--<label for="name">Username</label>--}}
                                <input type="text" name="public_name" id="public_name" placeholder="Public name"
                                       value="{{ old('public_name') }}">
                                @if ($errors->has('public_name'))
                                    <span class="error">
                                        <strong>{{ $errors->first('public_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                                {{--<label for="name">Username</label>--}}
                                <input type="text" name="url" id="url" placeholder="Public url for your personal page"
                                       value="{{ old('url') }}">
                                @if ($errors->has('url'))
                                    <span class="error">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="padding-box">
                            <div class="form-group{{ $errors->has('application_text') ? ' has-error' : '' }}">
                                {{--<label for="name">Username</label>--}}
                                <textarea name="application_text" id="application_text" placeholder="Please tell us about your experience as an author and why you want to join our team.">{{ old('application_text') }}</textarea>
                                @if ($errors->has('application_text'))
                                    <span class="error">
                                        <strong>{{ $errors->first('application_text') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row center-hz">
                    <div class="col-xs-10 col-sm-6 text-center">
                        <p>*To become an author, you must first get approved by News.com. After you register and verify your email address,
                            you're application will be reviewed within 72hrs. Minimum payments are 5$ and are paid the second Thursday of each month.
                            By clicking Register, you agree to our <a href="{{ route('terms') }}">Terms of Service</a>, <a href="{{ route('privacy') }}">Privacy Policy</a> and agree to follow our <a href="{{ route('guidelines') }}">Guidelines</a>.</p>
                    </div>
                    <div class="col-xs-12 col-sm-8 margin-top text-center">
                        <div class="form-group text-center">
                            <button type="submit" name="sign-up" id="sign-up" class="button">Register</button>
                            <a href="{{ route('login') }}" class="primary-colored margin-left">Login</a>
                        </div>
                    </div>
                </div>
            </form>
        {{--***** <div> ***** --}}
    @endcomponent
@endsection
