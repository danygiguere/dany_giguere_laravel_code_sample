@extends('layouts.app')

@section('content')
    @component('templates.main_without_sidebar')
        {{--***** <div class="col-xs-12 main-content"> ***** --}}
            <div class="row text-center padding-top-for-mobile">
                <div class="col-xs-12 title-section padding-bottom">
                    <h2>Login</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
                    <div class="padding-box">
                        <form id="login_form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {{--<label for="name">Email Address</label>--}}
                                <input type="email" name="email" id="email" placeholder="Email Address" value="{{ old('email') }}" autofocus>
                                @if ($errors->has('email'))
                                    <span class="error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                {{--<label for="password">Password</label>--}}
                                <input type="password" name="password" id="password" placeholder="Password" value="">
                                @if ($errors->has('password'))
                                    <span class="error">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="login" id="login" class="button">Login</button>
                                <div class="right padding-top">
                                    <a class="right" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        {{--***** <div> ***** --}}
    @endcomponent
@endsection
