import Vue from 'vue'

class Errors {
    constructor() {
        this.errors = {};
    }

    has(field) {
        if(this.errors.hasOwnProperty) {
            return this.errors.hasOwnProperty(field);
        }
    }

    any() {
        return Object.keys(this.errors).length > 0;
    }

    get(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
    }

    record(errors) {
        if(errors) {
            this.errors = errors;
        }
    }

    clear(field) {
        if (field) {
            Vue.delete(this.errors, field);
            return;
        }
        this.errors = {};
    }
}
export {Errors};
