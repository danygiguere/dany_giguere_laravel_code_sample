require('./bootstrap');
window.Vue = require('vue');
import { elementIsInViewport } from './functions';

Vue.component('menu-search', require('./components/MenuSearch'));
Vue.component('mobile-menu-search', require('./components/MobileMenuSearch'));
Vue.component('search-page', require('./components/SearchPage'));
Vue.component('lazyload-img', require('./components/LazyloadImg'));
Vue.component('adsense', require('./components/Adsense'));

Vue.component('modal', require('./components/dashboard/modals/Modal'));
Vue.component('delete-my-account', require('./components/dashboard/DeleteMyAccount'));
Vue.component('self-deactivate-user-modal', require('./components/dashboard/modals/SelfDeactivateUserModal'));
Vue.component('self-reactivate-user-modal', require('./components/dashboard/modals/SelfReactivateUserModal'));
Vue.component('publish-draft-modal', require('./components/dashboard/modals/PublishDraftModal'));
Vue.component('delete-draft-modal', require('./components/dashboard/modals/DeleteDraftModal'));
Vue.component('toggle-remove-post-modal', require('./components/dashboard/modals/ToggleRemovePostModal'));
Vue.component('cancel-changes-draft-modal', require('./components/dashboard/modals/CancelChangesDraftModal'));

Vue.component('dashboard-draft', require('./components/dashboard/Draft'));
Vue.component('dashboard-drafts', require('./components/dashboard/Drafts'));
Vue.component('dashboard-draft-images', require('./components/dashboard/DraftImages'));
Vue.component('dashboard-draft-youtube', require('./components/dashboard/DraftYoutube'));
Vue.component('dashboard-draft-vimeo', require('./components/dashboard/DraftVimeo'));
Vue.component('dashboard-views', require('./components/dashboard/Views'));
Vue.component('dashboard-payments', require('./components/dashboard/Payments'));
Vue.component('dashboard-profile', require('./components/dashboard/Profile'));
Vue.component('dashboard-public-image', require('./components/dashboard/PublicImage'));
Vue.component('dashboard-users-management', require('./components/dashboard/UsersManagement'));
Vue.component('dashboard-posts-management', require('./components/dashboard/PostsManagement'));
Vue.component('dashboard-settings', require('./components/dashboard/Settings'));

if (document.querySelector('#app')) {
    const app = new Vue({
        el: '#app'
    });
}


/*
 https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded
 The DOMContentLoaded event is fired when the initial HTML document has been completely loaded and parsed,
 without waiting for stylesheets, images, and subframes to finish loading. A very different event load
 should be used only to detect a fully-loaded page. It is an incredibly popular mistake to use load
 where DOMContentLoaded would be much more appropriate, so be cautious.
 */
window.addEventListener("load", function () {

});

document.addEventListener('DOMContentLoaded', function () {
    var isMobile = false;
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)))
        isMobile = true;

    // LAZYLOAD & FADE IN EFFECTS
    if (document.querySelector("img[data-src]")) {
        function addInView(img) {
            img.classList.add('in-view');
        }
        let lazyLoadImageDataSrc = function () {
            let imageDataSrc = document.querySelectorAll("img[data-src]");
            for (let item of imageDataSrc) {
                if (elementIsInViewport(item)) {
                    item.setAttribute("src", item.getAttribute("data-src"));
                    item.removeAttribute("data-src");
                    item.addEventListener('transitionend', addInView(item));
                }
            }

            // if all the images are loaded, stop calling the function
            if (imageDataSrc.length == 0) {
                window.removeEventListener("DOMContentLoaded", lazyLoadImageDataSrc);
                window.removeEventListener("load", lazyLoadImageDataSrc);
                window.removeEventListener("resize", lazyLoadImageDataSrc);
                window.removeEventListener("scroll", lazyLoadImageDataSrc);
            }
        };
        window.addEventListener("DOMContentLoaded", lazyLoadImageDataSrc);
        window.addEventListener("load", lazyLoadImageDataSrc);
        window.addEventListener("resize", lazyLoadImageDataSrc);
        window.addEventListener("scroll", lazyLoadImageDataSrc);
    }

    if (document.querySelector("img[data-loads-on-hover]")) {
        let menuBarLis = document.querySelectorAll(".menu-bar li");
        function addInView(img) {
            setTimeout(function () {
                img.classList.add('in-view');
            }, 20);
        }
        for (let menuBarLi of menuBarLis) {
            menuBarLi.addEventListener("mouseover", function () {
                let liImages = menuBarLi.querySelectorAll("img[data-loads-on-hover]");
                for (let img of liImages) {
                    img.setAttribute("src", img.getAttribute("data-loads-on-hover"));
                    img.removeAttribute("data-loads-on-hover");
                    img.addEventListener('transitionend', addInView(img));
                }
            });
        }
    }


    // SIDEBAR
    if (document.querySelector('.sidebar')) {
        function resizeMainContent() {
            let clientHeight = document.querySelector('.sidebar').clientHeight;
            let mainContent = document.querySelector('.main-content');
            mainContent.style.minHeight = clientHeight + "px";
        }
        window.addEventListener("resize", resizeMainContent);
        resizeMainContent();
    }


    // MENU
    if (document.querySelector('nav')) {
        let mobileMenu = document.querySelector('.mobile-menu');
        let mobileMenuButton = document.querySelector('.mobile-menu-button');
        let closeMobileMenu = document.querySelector('.close-mobile-menu');
        let mobileSearch = document.querySelector('.mobile-search');
        let mobileSearchButton = document.querySelector('.mobile-search-button');
        let closeMobileSearch = document.querySelector('.close-mobile-search');

        function toggleMenu() {
            mobileMenu.classList.toggle('open');
        }

        function toggleSearch() {
            mobileSearch.classList.toggle('open');
        }

        mobileMenuButton.addEventListener('click', toggleMenu);
        closeMobileMenu.addEventListener('click', toggleMenu);
        mobileSearchButton.addEventListener('click', toggleSearch);
        closeMobileSearch.addEventListener('click', toggleSearch);
    }
    // MENU UP/DOWN EFFECT
    if (document.querySelector('nav')) {
        let lastScrollTop = 0;
        window.addEventListener("scroll", function () {
            let pageYOffset = window.pageYOffset || document.documentElement.scrollTop;
            let mobileMenu = document.querySelector('nav');
            let menuBar = document.querySelector('.menu-bar');
            let topBar = document.querySelector('.top-bar');
            let header = document.querySelector('header');
            if (window.pageYOffset > 80) {
                if (pageYOffset > lastScrollTop) {
                    // scrolling down
                    mobileMenu.classList.remove('up');
                    mobileMenu.classList.add('down');
                }
                else {
                    // scrolling up
                    mobileMenu.classList.remove('down');
                    mobileMenu.classList.add('up');
                }
            }
            if (window.pageYOffset > (header.clientHeight + topBar.clientHeight)) {
                if (pageYOffset > lastScrollTop) {
                    // scrolling down
                    menuBar.classList.remove('up');
                    menuBar.classList.add('down');
                }
                else {
                    // scrolling up
                    menuBar.classList.remove('down');
                    menuBar.classList.add('up');
                }
            }
            else {
                menuBar.classList.remove('down');
                menuBar.classList.remove('up');
                menuBar.style.transitionDuration = '0s';
            }
            if (window.pageYOffset == 0) {
                mobileMenu.classList.remove('down');
                mobileMenu.classList.remove('up');
                mobileMenu.style.transitionDuration = '0s';
            }
            if (window.pageYOffset > 80) {
                mobileMenu.style.transitionDuration = '.2s';
            }
            if (window.pageYOffset > (header.clientHeight + topBar.clientHeight)) {
                menuBar.style.transitionDuration = '.2s';
            }
            lastScrollTop = pageYOffset;
        }, false);
    }
    if (document.querySelector('.menu-bar')) {
        let header_1 = document.querySelector('header');
        let menuBar_1 = document.querySelector('.menu-bar');
        let topBar_1 = document.querySelector('.top-bar');
        let logoContainer_1 = document.querySelector('.logo-container');

        function toggleHeaderFixed() {
            let windowOffsetTop = window.pageYOffset;
            if (windowOffsetTop >= (logoContainer_1.clientHeight + topBar_1.clientHeight + menuBar_1.clientHeight)) {
                menuBar_1.classList.add('fixed');
                header_1.style.marginBottom = menuBar_1.clientHeight + "px";
            }
            else if (windowOffsetTop <= (logoContainer_1.clientHeight + topBar_1.clientHeight)) {
                menuBar_1.classList.remove('fixed');
                header_1.style.marginBottom = "0px";
            }
        }

        window.addEventListener("resize", toggleHeaderFixed);
        window.addEventListener("scroll", toggleHeaderFixed);
        toggleHeaderFixed();
    }

});