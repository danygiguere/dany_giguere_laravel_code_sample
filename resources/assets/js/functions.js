export function elementIsInViewport(el) {
    let rect = el.getBoundingClientRect(),
        windowHeight = (window.innerHeight || document.documentElement.clientHeight),
        windowWidth = (window.innerWidth || document.documentElement.clientWidth);
    return (((rect.top >= -rect.height) && (rect.bottom <= (windowHeight + rect.height))) &&
        ((rect.left >= -rect.width) && (rect.right <= (windowWidth + rect.width))));
}

export function redirecIfBanned(response) {
    if(response.request.responseURL === 'http://news.test/banned-user') {
        window.location.reload();
    }
}

export function redirecIfError(response) {
    if(response.status === 403 || response.status === 404) {
        window.location.reload();
    }
}
