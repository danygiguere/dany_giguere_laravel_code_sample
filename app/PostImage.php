<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{
    protected $fillable = [
        'size', 'image_number', 'filename', 'order'
    ];
}
