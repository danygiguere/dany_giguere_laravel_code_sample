<?php

namespace App\ViewComposers;

use App\Category;
use App\Post;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // VIEW COMPOSER FOR THE MENU
        view()->composer('layouts._header', function ($view) {
            // get all the active categories
            $view->with('categories', Category::orderedByTrending());

            // get the top posts for all categories together
            $view->with('trendingPosts', Post::trending());

            // get the trending posts for all categories
            $view->with('trendingPostsForAllCategories', Post::trendingForAllCategories());
        });

        // VIEW COMPOSER FOR THE FOOTER
        view()->composer('layouts._footer', function ($view) {
            // latest news
            $view->with('latestPosts', Post::latests());

            // trending news
            $view->with('trendingPosts', collect(Post::trending())->take(3));

            // suggestions
            $view->with('suggestionsPosts', Post::suggestions());
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}