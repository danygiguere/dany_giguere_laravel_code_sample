<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostRemovedEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $post;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $post)
    {
        $this->user = $user;
        $this->post = $post;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //if I don't add the from, it will used what's in config.mail.from.address so I can remove from...
        return $this->from(config('mail.from.address'), 'News.test')
            ->subject("Your recent post has been removed")
            ->markdown('emails.post_removed');
    }
}
