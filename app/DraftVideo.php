<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DraftVideo extends Model
{
    protected $fillable = [
        'type', 'video_id'
    ];
}
