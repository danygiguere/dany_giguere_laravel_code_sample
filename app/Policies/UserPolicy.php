<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can views his posts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewPosts(User $user)
    {
        if($user->status == 'Rejected' || $user->status == 'Banned' || $user->self_deactivated_at != NULL) {
            return false;
        }
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can update his profile.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function updateProfile(User $user)
    {
        if($user->status == 'Rejected' || $user->status == 'Banned' || $user->self_deactivated_at != NULL) {
            return false;
        }
        return true;
    }

    /**
     * Determine whether the user can update his settings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function updateSettings(User $user)
    {
        if($user->status == 'Rejected' || $user->status == 'Banned') {
            return false;
        }
        return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        //
    }
}
