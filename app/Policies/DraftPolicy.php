<?php

namespace App\Policies;

use App\Post;
use App\User;
use App\Draft;
use Illuminate\Auth\Access\HandlesAuthorization;

class DraftPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if($user->status == 'Rejected' || $user->status == 'Banned' || $user->self_deactivated_at != NULL) {
            return false;
        }
    }

    /**
     * Determine whether the user can view the draft.
     *
     * @param  \App\User  $user
     * @param  \App\Draft  $draft
     * @return mixed
     */
    public function view(User $user, Draft $draft)
    {
        return $user->id === $draft->user_id;
    }

    /**
     * Determine whether the user can create drafts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->status != 'Suspended' && $user->hasRoleOrHigher('Author');
    }

    /**
     * Determine whether the user can update the draft.
     *
     * @param  \App\User  $user
     * @param  \App\Draft  $draft
     * @return mixed
     */
    public function update(User $user, Draft $draft)
    {
        return $user->id === $draft->user_id;
    }

    /**
     * Determine whether the user can delete the draft.
     *
     * @param  \App\User  $user
     * @param  \App\Draft  $draft
     * @return mixed
     */
    public function delete(User $user, Draft $draft)
    {
        return $user->id === $draft->user_id;
    }

    /**
     * Determine whether the user can publish the draft.
     *
     * @param  \App\User  $user
     * @param  \App\Draft  $draft
     * @return mixed
     */
    public function publish(User $user, Draft $draft)
    {
        if($post = Post::find($draft->id)) {
            return $user->id === $draft->user_id;
        }
        return $user->hasRoleOrHigher('Author');
    }
}
