<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DraftImage extends Model
{
    protected $fillable = [
        'copied_to_post', 'size', 'image_number', 'filename', 'order'
    ];

}
