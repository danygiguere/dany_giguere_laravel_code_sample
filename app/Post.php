<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Builder;

class Post extends Model
{
    // for scout/algolia
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'title', 'text', 'slug', 'category_id', 'user_id', 'media_type', 'image'
    ];

    protected $casts = [
        'self_deactivated' => 'boolean',
    ];

    // for soft delete
    protected $dates = ['deleted_at'];

    /******** Relationships ********/
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function postImages()
    {
        return $this->hasMany('App\PostImage')->orderBy('order', 'asc');
    }

    public function postVideos()
    {
        return $this->hasMany('App\PostVideo');
    }

    public function toSearchableArray()
    {
        /**
         * Load the categories relation so that it's available by algolia
         *  in the laravel toArray method
         */
        $this->user;
        $this->category;

        return $this->toArray();
    }

    /******** Scopes ********/
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('self_deactivated', 0);
        });
    }

    /******** Accessors & Mutators ********/
    public function getPublicationDateAttribute()
    {
        $publicationDate = $this->created_at->diffInDays(\Carbon\Carbon::now()) >= 7 ? $this->created_at->format('M j, Y') : $this->created_at->diffForHumans();
        return $publicationDate;
    }

    public function getExcerptTitleAttribute()
    {
        $excerptTitle = str_limit(strip_tags($this->title), 40, '...');
        return $excerptTitle;
    }

    public function getExcerptTextAttribute()
    {
        $text = str_replace('><', '> <', $this->text);
        $excerptText = str_limit(strip_tags($text), 150, '...');
        return $excerptText;
    }

    // This method should be cached when used
    public function getAllImagesAttribute()
    {
        try {
            $largeImage = [];
            foreach ($this->postImages as $image) {
                if (Storage::disk('s3')->exists("posts/large/".$image->filename)) {
                    $largeImage[] = Storage::disk('s3')->url("posts/large/".$image->filename);
                } else {
                    $largeImage[] = '/images/image_not_available.png';
                }
            }
        } catch (\Exception $e) {
            $largeImage = '/images/image_not_available.png';
        }
        return $largeImage;
    }

    // This method should be cached when used
    public function getExcerptImage($size = 'large')
    {
        try {
            if ($this->media_type == "youtube" || $this->media_type == "vimeo") {
                $excerptImage = getMediaImageUrl($this->media_type, $this->postVideos->where('type', $this->media_type)->first()->video_id);
            } elseif(Storage::disk('s3')->exists("posts/$size/".$this->postImages->where('order', 0)->first()->filename)) {
                $excerptImage = Storage::disk('s3')->url("posts/$size/".$this->postImages->where('order', 0)->first()->filename);
            } else {
                $excerptImage = '/images/image_not_available.png';
            }
        } catch (\Exception $e) {
            $excerptImage = '/images/image_not_available.png';
        }
        return $excerptImage;
    }

    // This method should be cached when used
    public function getLargeImageAttribute()
    {
        return $this->getExcerptImage();
    }

    // This method should be cached when used
    public function getMediumImageAttribute()
    {
        return $this->getExcerptImage('medium');
    }

    // This method should be cached when used
    public function getSmallImageAttribute()
    {
        return $this->getExcerptImage('small');
    }

    /******** Methods with Redis caching ********/
    public static function getCachedPost($postId)
    {
        return Cache::tags(["post_$postId"])->remember("post_$postId", config('app.redis_caching_time'), function () use ($postId) {
            $post = Post::where('id', $postId)->with('user', 'category', 'postImages', 'postVideos')->first();
            $post->excerpt_text = $post->excerpt_text;
            $post->excerpt_title = $post->excerpt_title;
            if($post->media_type == 'image') {
                $post->all_images = $post->all_images;
                $post->large_image = $post->large_image;
                $post->medium_image = $post->medium_image;
                $post->small_image = $post->small_image;
            }
            if($post->media_type == 'youtube' or $post->media_type == 'vimeo') {
                $post->video_url = getVideoUrl($post->media_type, $post->postVideos()->where('type', $post->media_type)->first()->video_id, false);
            }
            return $post;
        });
    }

    public static function getDateOfThirtiethPost()
    {
        return Cache::tags(['posts'])->remember('date_of_thirtieth_post', config('app.redis_caching_time_medium'), function () {
            return Post::select('created_at')->orderBy('created_at', 'desc')->take(30)->get()->last()->created_at;
        });
    }

    // get the 6 most trending posts among all categories together. Calculated from the date of the 30th post.
    public static function trending()
    {
        return Cache::tags(['posts'])->remember('trending_posts', config('app.redis_caching_time_medium'), function () {
            $postsIds = Post::leftJoin('views', 'posts.id', '=', 'views.post_id')
                ->select('posts.id', DB::raw('IFNULL(SUM(views.total), 0) as total_views'))
                ->where('posts.created_at', '>=', Post::getDateOfThirtiethPost())
                ->groupBy('posts.id')
                ->orderBy('total_views', 'desc')
                ->orderBy('posts.created_at', 'desc')
                ->take(6)->pluck('id');
            foreach($postsIds as $postId) {
                $posts[] = Post::getCachedPost($postId);
            }
            return $posts;
        });
    }

    public static function trendingForCategory($categoryId)
    {
        return Cache::tags(['posts'])->remember("trending_posts_for_category_$categoryId", config('app.redis_caching_time_medium'), function () use ($categoryId) {
            $daysBack = Post::select('created_at')->where('category_id', '=', $categoryId)->orderBy('created_at', 'desc')->take(30)->get()->last()->created_at;
            $postsIds = Post::leftJoin('views', 'posts.id', '=', 'views.post_id')
                ->select('posts.id', DB::raw('IFNULL(SUM(views.total), 0) as total_views'))
                ->where('posts.category_id', '=', $categoryId)
                ->where('posts.created_at', '>=', $daysBack)
                ->groupBy('posts.id')
                ->orderBy('total_views', 'desc')
                ->orderBy('posts.created_at', 'desc')
                ->take(6)->pluck('id');
            foreach($postsIds as $postId) {
                $posts[] = Post::getCachedPost($postId);
            }
            return $posts;
        });
    }

    public static function trendingForAllCategories()
    {
        return Cache::tags(['posts'])->remember('trending_posts_for_all_categories', config('app.redis_caching_time_medium'), function () {
            foreach (Category::orderedByTrending() as $trendingCategory) {
                $trendingPostsForAllCategories[] = Post::trendingForCategory($trendingCategory->id);
            }
            return $trendingPostsForAllCategories;
        });
    }

    public static function latests()
    {
        return Cache::tags(['posts'])->remember('latest_posts', config('app.redis_caching_time_medium'), function () {
            $postsIds = Post::orderBy('created_at', 'desc')->take(3)->pluck('id');
            foreach($postsIds as $postId) {
                $posts[] = Post::getCachedPost($postId);
            }
            return $posts;
        });
    }

    public static function latestsForCategory($categoryId)
    {
        return Cache::tags(['posts'])->remember("latests_posts_for_category_$categoryId", config('app.redis_caching_time_medium'), function () use ($categoryId) {
            $postsIds = Post::where('category_id', '=', $categoryId)->orderBy('created_at', 'desc')->take(18)->pluck('id');
            foreach($postsIds as $postId) {
                $posts[] = Post::getCachedPost($postId);
            }
            return $posts;
        });
    }

    public static function suggestions()
    {
        return Cache::tags(['posts'])->remember('suggestions_posts', config('app.redis_caching_time_medium'), function () {
            $postsIds = Post::where('created_at', '>=', Carbon::today()->subDays(100))->inRandomOrder()->take(3)->pluck('id');
            foreach($postsIds as $postId) {
                $posts[] = Post::getCachedPost($postId);
            }
            return $posts;
        });
    }

    public static function byThisAuthor($userId)
    {
        return Cache::tags(['posts'])->remember("by_this_author_$userId", config('app.redis_caching_time_medium'), function () use ($userId) {
            $postsIds = Post::where('user_id', '=', $userId)->inRandomOrder()->take(3)->pluck('id');
            foreach($postsIds as $postId) {
                $posts[] = Post::getCachedPost($postId);
            }
            return $posts;
        });
    }

    public static function totalViewsForPost($postId)
    {
        return Cache::tags(["post_$postId"])->remember("total_views_for_post_$postId", config('app.redis_caching_time_short'), function () use ($postId) {
            return DB::table('views')->select(DB::raw('SUM(total) as total_views'))->where('post_id', $postId)->first()->total_views;
        });
    }

    /******** Copy Post to Draft Methods ********/
    public function copyPostToDraft($draft)
    {
        $draft->media_type = $this->media_type;
        $draft->title = $this->title;
        $draft->text = $this->text;
        if ($this->media_type == 'image') {
            $this->copyImagesToDraft($draft);
        } else {
            $this->copyVideoToDraft($draft);
        }
        $draft->unsaved_changes = false;
        $draft->save();
        return ['message' => __('validation.draft.cancel-changes.success-message')];
    }

    public function copyImagesToDraft($draft): void
    {
        $draft->deleteVideos();
        $draft->deleteImages();
        foreach ($this->postImages as $postImage) {
            Storage::disk('s3')->copy("posts/large/$postImage->filename", "drafts/large/$postImage->filename");
            Storage::disk('s3')->copy("posts/medium/$postImage->filename", "drafts/medium/$postImage->filename");
            Storage::disk('s3')->copy("posts/small/$postImage->filename", "drafts/small/$postImage->filename");

            $draft->draftImages()->create([
                'copied_to_post' => true,
                'image_number' => $postImage->image_number,
                'filename' => $postImage->filename,
                'order' => $postImage->order
            ]);
        }
    }

    public function copyVideoToDraft($draft): void
    {
        $draft->deleteVideos();
        $draft->deleteImages();
        $postVideo = $this->postVideos()->where('type', $this->media_type)->first();
        $draft->draftVideos()->where('type', $this->media_type)->updateOrCreate([
            'type' => $postVideo->type,
            'video_id' => $postVideo->video_id
        ]);
    }

    public function removeDeletedDraftImagesFromPost($draft): void
    {
        if(collect($draft->image_numbers_to_be_deleted_from_post)->isNotEmpty()) {
            foreach ($draft->image_numbers_to_be_deleted_from_post as $image_number) {
                if($postImage = $this->postImages()->where('image_number', $image_number)->first()) {
                    $this->deleteImageFromS3($postImage->filename);
                    $postImage->forceDelete();
                }
            }
            $draft->image_numbers_to_be_deleted_from_post = NULL;
            $draft->save();
        }
    }

    /******** Delete Images Methods ********/
    public function deleteImageFromS3($filename): void
    {
        Storage::disk('s3')->delete("posts/large/$filename");
        Storage::disk('s3')->delete("posts/medium/$filename");
        Storage::disk('s3')->delete("posts/small/$filename");
    }

    public function deleteImagesFromS3(): void
    {
        foreach ($this->postImages as $postImage) {
            $this->deleteImageFromS3($postImage->filename);
        }
    }

    public function deleteImage($postImage)
    {
        $this->deleteImageFromS3($postImage->filename);
        $postImage->forceDelete();
    }

    public function deleteImages()
    {
        foreach ($this->postImages as $postImage) {
            $this->deleteImage($postImage);
        }
    }

    /******** Delete Videos Methods ********/
    public function deleteVideos()
    {
        foreach($this->postVideos as $postVideo) {
            $postVideo->forceDelete();
        }
    }

}
