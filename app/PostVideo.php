<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostVideo extends Model
{
    protected $fillable = [
        'type', 'video_id'
    ];
}
