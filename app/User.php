<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'auth_token', 'verification_token', 'verification_token_created_at', 'application_text',
        'image', 'public_name', 'url', 'title', 'description', 'public_email', 'public_email_2', 'website', 'website_2', 'website_3',
        'facebook', 'twitter', 'google_plus', 'youtube', 'vimeo', 'instagram', 'real_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'auth_token', 'verification_token', 'verification_token_created_at', 'application_text'
    ];

    public function getRouteKeyName()
    {
        return 'url';
    }

    /******** Relationships ********/
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function drafts()
    {
        return $this->hasMany('App\Draft');
    }

    /******** Roles ********/
    // available roles: Unverified, Applicant, Author, Admin, Superadmin

    public function getIsSuperadminAttribute()
    {
        return $this->role === 'Superadmin';
    }

    public function getIsAdminAttribute()
    {
        return $this->role === 'Admin';
    }

    public function getIsAuthorAttribute()
    {
        return $this->role === 'Author';
    }

    public function getIsApplicantAttribute()
    {
        return $this->role === 'Applicant';
    }

    public function hasRoleOrHigher($role)
    {
        $response = false;
        if($role === 'Applicant') {
            if ($this->role == 'Applicant' || $this->role == 'Author' || $this->role == 'Admin' || $this->role == 'Superadmin') {
                $response = true;
            }
        } elseif($role === 'Author') {
            if($this->role == 'Author' || $this->role == 'Admin' || $this->role == 'Superadmin') {
                $response = true;
            }
        } elseif($role === 'Admin') {
            if($this->role == 'Admin' || $this->role == 'Superadmin') {
                $response = true;
            }
        } elseif($role === 'Superadmin') {
            if ($this->role == 'Superadmin') {
                $response = true;
            }
        }
        return $response;
    }

    /******** Status ********/
    // available status: Rejected, Active, Suspended, Banned

    public function getIsBannedAttribute()
    {
        return $this->status === 'Banned';
    }

    public function getIsBannedOrSelfDeactivatedAttribute()
    {
        return $this->status === 'Banned' || $this->self_deactivated_at != NULL;
    }

    public function getIsSelfDeactivatedAttribute()
    {
        return $this->self_deactivated_at != NULL;
    }

    public function getIsSuspendedAttribute()
    {
        return $this->status === 'Suspended';
    }

    /******** Scopes ********/


    /******** Accessors & Mutators ********/
    public function getDescriptionExcerptAttribute()
    {
        $descriptionExcerpt = str_limit(strip_tags($this->description), 100);
        return $descriptionExcerpt;
    }

    public function getImageAttribute($value)
    {
        try {
            if ($value == NULL or !Storage::disk('s3')->exists($value)) {
                $value = '/images/avatar.jpg';
            }
        } catch (\Exception $e) {
            $value = '/images/avatar.jpg';
        }
        return $value;
    }

    public function getImageFullUrlAttribute()
    {
        try {
            if ($this->image == NULL or !Storage::disk('s3')->exists($this->image)) {
                $image = '/images/avatar.jpg';
            } else {
                $image = Storage::disk('s3')->url($this->image);
            }
        } catch (\Exception $e) {
            $image = '/images/avatar.jpg';
        }
        return $image;
    }


    /******** Other methods ********/
    public function createVerificationToken(): void
    {
        $this->verification_token = createToken();
        $this->verification_token_created_at = Carbon::now();
        $this->save();
    }


}
