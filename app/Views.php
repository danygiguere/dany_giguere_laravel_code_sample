<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Views extends Model
{
    protected $fillable = [
        'post_id', 'views'
    ];
}
