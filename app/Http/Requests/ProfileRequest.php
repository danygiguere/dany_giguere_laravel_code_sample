<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'public_name' => 'required|max:255',
            'url' => 'required|unique:users,url,'.Auth::user()->id.'|max:255',
            'title' => 'max:255',
            'description' => 'max:255',
            'public_email' => 'email|max:255',
            'public_email_2' => 'email|max:255',
            'website' => 'max:255',
            'website_2' => 'max:255',
            'website_3' => 'max:255',
            'facebook' => 'max:255',
            'twitter' => 'max:255',
            'google_plus' => 'max:255',
            'youtube' => 'max:255',
            'vimeo' => 'max:255',
            'instagram' => 'max:255',
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
