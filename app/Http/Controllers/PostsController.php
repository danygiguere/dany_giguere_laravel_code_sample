<?php

namespace App\Http\Controllers;

use App\Draft;
use App\Jobs\SendPostRemovedEmail;
use App\Post;
use App\Views;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['only' => [
            'toggleRemove'
        ]]);
        $this->middleware('check_role:Admin', ['only' => [
            'toggleRemove'
        ]]);
    }

    public function show($category, $post_slug, $post_id)
    {
        try {
            $post = Post::getCachedPost($post_id);
            $post_slug === $post->slug or abort(404);
            // cookie to verify if we should increase post views
            if (Cookie::get('post-' . $post->id) != 'visited') {
                $views = Views::firstOrNew(
                    ['post_id' => $post->id, 'created_at' => today()->todatestring()]
                );
                $views->total += 1;
                $views->save();
                Cookie::queue('post-' . $post->id, 'visited', 30);
            }
            $totalViews = Post::totalViewsForPost($post->id);
            $postsByThisAuthor = Post::byThisAuthor($post->user_id);
            return view('posts.show', compact('post', 'totalViews', 'postsByThisAuthor'));
        } catch (\Exception $e) {
            return abort(404);
        }
    }

    public function toggleRemove(Request $request)
    {
        $postId = $request->get('post_id');
        $post = Post::withTrashed()->where('id', $postId)->with('user')->first();
        $this->authorize('remove', $post);
        $draft = Draft::withTrashed()->where('id', $post->id)->first();
        if($post->deleted_at == null) {
            $post->delete();
            $draft->delete();
            $post->user()->increment('posts_removed_count');
            SendPostRemovedEmail::dispatch($post->user, $post);
        } else {
            $post->restore();
            $draft->restore();
            $post->user()->decrement('posts_removed_count');
        }
        Cache::tags(["post_$post->id", "posts", "categories"])->flush();
        return $post;
    }

}
