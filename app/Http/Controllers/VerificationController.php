<?php

namespace App\Http\Controllers;

use App\Jobs\SendUserSignedUpEmail;
use App\Jobs\SendUserVerifyEmail;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class VerificationController extends Controller
{
    protected $user;

    public function verificationEmailSent()
    {
        $this->redirectIfWithoutSessionKey('can_view_verification_email_sent_page');
        return view('verify.verification_email_sent');
    }

    public function verificationEmailResent()
    {
        $this->redirectIfWithoutSessionKey('can_view_verification_email_resent_page');
        return view('verify.verification_email_resent');
    }

    public function verifyEmail($verification_token)
    {
        $user = $this->redirectIfVerificationTokenIsInvalid($verification_token);
        $this->redirectToLoginIfAlreadyVerified($user->role);
        $user->role = 'Applicant';
        $user->save();
        dispatch(new SendUserSignedUpEmail($user));
        session(['can_view_email_verified_page' => true]);
        return redirect()->route('verify.emailVerified');
    }

    // if user tries to login and is not verified
    public function emailNotVerified($auth_token)
    {
        $this->verifyUserAuthToken($auth_token);
        return view('verify.email_not_verified', compact('auth_token'));
    }

    /* Creating a resend verification email in case a user lost his link (when user logs in) */
    public function resendVerificationEmail($auth_token)
    {
        $user = $this->verifyUserAuthToken($auth_token);
        $this->redirectToLoginIfAlreadyVerified($user->role);
        $user->createVerificationToken();
        dispatch(new SendUserVerifyEmail($user));
        session(['can_view_verification_email_resent_page' => true]);
        return redirect()->route('verify.verificationEmailResent');
    }

    public function emailVerified()
    {
        $this->redirectIfWithoutSessionKey('can_view_email_verified_page');
        return view('verify.email_verified');
    }

    public function bannedUser()
    {
        $this->redirectIfWithoutSessionKey('can_view_banned_user_page');
        return view('verify.banned_user');
    }

    // general methods
    public function redirectIfVerificationTokenIsInvalid($verification_token)
    {
        if ($user = User::where('verification_token', $verification_token)->first()) {
            if($user->verification_token_created_at < Carbon::now()->subDays(2)) {
               return Redirect::to('/email-not-verified/' . $user->auth_token)->send();
            }
            return $user;
        } else {
            return abort(403);
        }
    }

    public function verifyUserAuthToken($auth_token)
    {
        if ($user = User::where('auth_token', $auth_token)->first()) {
            return $user;
        } else {
            return abort(403);
        }
    }

    public function redirectIfWithoutSessionKey($session_key)
    {
        if (session($session_key) != true) {
            return abort(403);
        }
    }

    public function redirectToLoginIfAlreadyVerified($role)
    {
        if ($role != 'Unverified') {
            return abort(403);
        }
    }

}
