<?php

namespace App\Http\Controllers;

use App\Category;
use App\PolicyAgreement;
use App\Post;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => [
            'policyAgreement',
            'agreeToPolicyAgreement'
        ]]);
    }

    public function home()
    {
        $trendingPosts = collect(Post::trending())->take(5);
        $trendingPostsForAllCategories = Post::trendingForAllCategories();
        return view('home', compact('trendingPosts','trendingPostsForAllCategories'));
    }

    public function category(Category $category)
    {
        $trendingPosts = collect(Post::trendingForCategory($category->id))->take(5);
        $latestPostsForCategory = Post::latestsForCategory($category->id);
        return view('category', compact('trendingPosts', 'latestPostsForCategory'));
    }

    public function policyAgreement() {
        return view('policy_agreement');
    }

    public function agreeToPolicyAgreement() {
        $user = Auth::user();
        $policyAgreementNumber = PolicyAgreement::currentPolicyNumber();
        $user->last_agreed_policy = $policyAgreementNumber;
        $user->save();
        $policyAgreement = new PolicyAgreement;
        $policyAgreement->policy_number = $policyAgreementNumber;
        $policyAgreement->user_id = $user->id;
        $policyAgreement->save();
        return redirect()->route('dashboard');
    }

    public function privacy() {
        return view('privacy');
    }

    public function terms() {
        return view('terms');
    }

    public function guidelines() {
        return view('guidelines');
    }

}
