<?php

namespace App\Http\Controllers;

use App\Category;
use App\Draft;
use App\Http\Requests\PersonalInfoRequest;
use App\Http\Requests\ProfileImageRequest;
use App\Http\Requests\ProfileRequest;
use App\Post;
use App\Rules\PasswordMatch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Cache;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('redirect_if_banned');
        $this->middleware('needs_to_agree_to_policy_agreement');
        $this->middleware('check_role:Superadmin', ['only' => [
            'categories',
            'storeCategories'
        ]]);
        $this->middleware('check_role:Admin', ['only' => [
            'postsManagement',
            'searchPostsForAllUsers',
            'usersManagement',
            'searchUsers'
        ]]);
        $this->middleware('check_role:Author', ['only' => [
            'posts',
            'searchPosts',
            'views',
            'searchViews',
            'profile',
            'updateProfile',
            'saveProfileImage',
            'settings',
            'updatePersonalInfo',
            'updatePassword',
            'payments'
        ]]);
    }

    public function summary()
    {
        $user = Auth::user();
        $today = today()->toDateString();
        $suspendedUntil = Carbon::create($user->suspended_at)->addHours(72);
        $numberOfPostsToday = $user->posts()->where('created_at', '>=', $today)->get()->count();
        $viewsForPostsPublishedToday = DB::table('views')
            ->join('posts', 'posts.id', '=', 'views.post_id')
            ->select(DB::raw('SUM(views.total) as total_views'))
            ->where('posts.user_id', $user->id)
//            ->where('views.created_at', '>=', $today)
            ->where('posts.created_at', '>=', $today)
            ->first()->total_views;
        return view('dashboard.summary', compact('user', 'suspendedUntil', 'numberOfPostsToday', 'viewsForPostsPublishedToday'));
    }

    public function searchPosts(Request $request)
    {
        $query_string = $request->get('query');
        $with_unsaved_changes = $request->get('with_unsaved_changes');
        $neverPublished = $request->get('never_published');
        $published = $request->get('published');
        $user = Auth::user();

        $drafts = Draft::where('user_id', $user->id)
            ->when($query_string != '*', function ($query) use ($query_string) {
                return $query->where(function ($query) use ($query_string) {
                    $query->where('title', 'like', '%' . $query_string . '%')
                        ->orWhere('text', 'like', '%' . $query_string . '%');
                });
            })
            ->when($with_unsaved_changes == 'true', function ($query) {
                return $query->where('unsaved_changes', 1);
            })
            ->when($neverPublished == 'true', function ($query) {
                return $query->where('first_published_on', NULL);
            })
            ->when($published == 'true', function ($query) {
                return $query->where('first_published_on', '!=', NULL);
            })
            ->orderBy('created_at', 'desc')->paginate(8);

        foreach ($drafts as $draft) {
            $draft->userPublicName = $draft->user->public_name;
            $draft->createdOn = $draft->created_on;
            $draft->publicationDate = $draft->publication_date;
            $draft->image = $draft->image;
            $draft->title = $draft->excerptTitle;
            $draft->route = action('PostsController@show', ['category' => $draft->category->url, 'post_slug' => $draft->slug, 'post' => $draft->id]);
        }
        return $drafts;
    }

    public function searchUsers(Request $request)
    {
        $query_string = $request->get('query');
        $role = $request->get('role');
        $status = $request->get('status');
        $self_deactivated = $request->get('self_deactivated');

        $users = User::when($query_string != '*', function ($query) use ($query_string) {
            return $query->where(function ($query) use ($query_string) {
                $query->where('public_name', 'like', "%$query_string%")
                    ->orWhere('real_name', 'like', "%$query_string%")
                    ->orWhere('email', 'like', "%$query_string%");
            });
        })
            ->when($role != 'All', function ($query) use ($role) {
                return $query->where('role', $role);
            })
            ->when($status != 'All', function ($query) use ($status) {
                return $query->where('status', $status);
            })
            ->when($self_deactivated == 'true', function ($query) use ($self_deactivated) {
                return $query->where('self_deactivated_at', '!=', NULL);
            })
            ->orderBy('created_at', 'desc')->paginate(15);
        return $users;
    }

    public function searchPostsForAllUsers(Request $request)
    {
        $query_string = $request->get('query');
        $removed = $request->get('removed');
        $self_deactivated = $request->get('self_deactivated') == 'true' ? 1 : 0;

        $posts = Post::withoutGlobalScopes()
            ->when($removed == 'true', function ($query) {
                return $query->where('deleted_at', '!=', NULL)->where('self_deactivated', 0);
            }, function ($query) {
                return $query->where('deleted_at', NULL);
            })
            ->where('self_deactivated', $self_deactivated)
            ->when($query_string != '*', function ($query) use ($query_string) {
                return $query->where(function ($query) use ($query_string) {
                    $query->where('title', 'like', '%' . $query_string . '%')
                        ->orWhere('text', 'like', '%' . $query_string . '%');
                });
            })
            ->orderBy('created_at', 'desc')->paginate(8);

        foreach ($posts as $post) {
            $post->userPublicName = $post->user->public_name;
            $post->userUrl = action('UsersController@show', ['user' => $post->user]);
            $post->publicationDate = $post->publication_date;
            $post->mediumImage = $post->medium_image;
            $post->title = $post->excerptTitle;
            $post->excerpt = $post->excerptText;
            $post->route = action('PostsController@show', ['category' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]);
        }
        return $posts;
    }

    public function posts()
    {
        $this->authorize('viewPosts', Auth::user());
        return view('dashboard.drafts');
    }

    public function views()
    {
        $views = $this->searchViews();
        return view('dashboard.views', compact('views'));
    }

    public function searchViews($page = 1, $daysBack = 0)
    {
        $page = request()->get('page');
        $daysBack = request()->get('daysBack');
        $userId = Auth::id();
        $fromDate = Carbon::today()->subDays($daysBack)->toDateString();
        $numberOfViewsPerPage = 15;

        $views = DB::table('views')
            ->join('posts', 'posts.id', '=', 'views.post_id')
            ->join('categories', 'posts.category_id', '=', 'categories.id')
            ->select('posts.id', DB::raw('SUM(views.total) as total_views'), 'posts.created_at', 'categories.url', 'posts.slug')
            ->where('views.created_at', '>=', $fromDate)
            ->where('posts.user_id', $userId)
            ->groupBy('posts.id')
            ->orderBy('total_views', 'desc')
            ->skip($page * $numberOfViewsPerPage - $numberOfViewsPerPage)->take($numberOfViewsPerPage)->get();

        $viewsCount = DB::table('views')
            ->join('posts', 'posts.id', '=', 'views.post_id')
            ->select('posts.id')
            ->where('views.created_at', '>=', $fromDate)
            ->where('posts.user_id', $userId)
            ->groupBy('views.id')
            ->count();

        $lastPage = intval(ceil($viewsCount / $numberOfViewsPerPage));
        $collection = collect(['views' => $views, 'lastPage' => $lastPage]);
        $views = $collection->toJson();

        return $views;
    }

    public function profile()
    {
        $user = Auth::user();
        $this->authorize('updateProfile', $user);
        $user->imageFullUrl = $user->image_full_url;
        return view('dashboard.profile', compact('user'));
    }

    public function updateProfile(ProfileRequest $request)
    {
        $user = Auth::user();
        $this->authorize('updateProfile', $user);
        $user->fill($request->only('public_name',
            'url',
            'title',
            'description',
            'public_email',
            'public_email_2',
            'website',
            'website_2',
            'website_3',
            'facebook',
            'twitter',
            'google_plus',
            'youtube',
            'vimeo',
            'instagram'
        ));
        $user->save();
        return ['message' => __('validation.dashboard.profile.success-message')];
    }

    private function containAndSaveImage($file, $filename, $width, $height)
    {
        $manager = new ImageManager(array('driver' => 'gd'));
        $image = $manager->make($file)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->resizeCanvas($width, $height, 'center', false, '1b1b1b');
        $imageStream = $image->stream();
        Storage::disk('s3')->put("users/$filename", $imageStream->__toString());
        return $image;
    }

    public function saveProfileImage(ProfileImageRequest $request)
    {
        $user = Auth::user();
        $this->authorize('updateProfile', $user);
        Storage::disk('s3')->delete($user->image);
        $file = $request->file('image');
        $filename = strtolower($user->url) . "_$user->id." . $file->getClientOriginalExtension();
        $this->containAndSaveImage($file, $filename, 120, 120);
        $user->image = "users/" . $filename;
        $user->save();
        return [
            'message' => __('validation.dashboard.profile-image.success-message'),
            'imageFullUrl' => $user->image_full_url
        ];
    }

    public function settings()
    {
        $user = Auth::user();
        $this->authorize('updateSettings', $user);
        $user = collect($user)->only(['id', 'real_name', 'email', 'role', 'status', 'self_deactivated', 'self_deactivated_at', 'created_at']);
        return view('dashboard.settings', compact('user'));
    }

    public function updatePersonalInfo(PersonalInfoRequest $request)
    {
        $user = Auth::user();
        $this->authorize('updateSettings', $user);
        $user->fill($request->only('real_name', 'email'));
        $user->save();
        return ['message' => __('validation.dashboard.personal-info.success-message')];
    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        $this->authorize('updateSettings', $user);
        $request->validate([
            'current_password' => ['required', new PasswordMatch],
            'password' => 'confirmed|min:8|max:255',
        ]);
        $user->password = Hash::make($request->password);
        $user->save();
        return ['message' => __('validation.dashboard.password.success-message')];
    }

    public function payments()
    {
        return view('dashboard.payments');
    }

    public function usersManagement()
    {
        return view('dashboard.users_management');
    }

    public function postsManagement()
    {
        return view('dashboard.posts_management');
    }

    public function categories()
    {
        $allCategories = Category::all();
        return view('dashboard.categories', compact('allCategories'));
    }

    public function storeCategories(Request $request)
    {
        Category::create([
            'name' => $request->name,
            'url' => $request->url
        ]);
        Cache::tags(['categories'])->flush();
        return back();
    }

}
