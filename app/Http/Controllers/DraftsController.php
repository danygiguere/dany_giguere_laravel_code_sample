<?php

namespace App\Http\Controllers;

use App\Category;
use App\Draft;
use App\DraftImage;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class DraftsController extends Controller
{
    public function create()
    {
        $this->authorize('create', Draft::class);
        $user = Auth::user();
        $draft = $user->drafts()->create();
        return redirect()->route('drafts.edit', compact('draft'));
    }

    public function edit(Draft $draft)
    {
        $this->authorize('update', $draft);
        $allCategories = Category::all();
        $draftImages = $this->mapImagePaths($draft);
        $draftYoutubeVideo = collect($draft->draftVideos->where('type', 'youtube')->first());
        $draftVimeoVideo = collect($draft->draftVideos->where('type', 'vimeo')->first());
        return view('dashboard.draft', compact('draft', 'allCategories', 'draftImages', 'draftYoutubeVideo', 'draftVimeoVideo'));
    }

    public function updateCategory(Request $request, Draft $draft)
    {
        $this->authorize('update', $draft);
        if($draft->first_published_on == null) {
            $request->validate([
                'category_id' => 'required|exists:categories,id'
            ]);
            $draft->category_id = $request->category_id;
            $draft->unsaved_changes = true;
            $draft->save();
            $message = __('validation.draft.category.success-message');
        } else {
            $message = 'cannot change category once published';
        }
        return [
            'message' => $message
        ];
    }

    public function updateMediaType(Request $request, Draft $draft)
    {
        $this->authorize('update', $draft);
        $request->validate([
            'media_type' => 'required|in:image,youtube,vimeo',
        ]);
        $draft->media_type = $request->media_type;
        $draft->unsaved_changes = true;
        $draft->save();
        return [
            'message' => __('validation.draft.media-type.success-message')
        ];
    }

    public function updateTitle(Request $request, Draft $draft)
    {
        $this->authorize('update', $draft);
        $request->validate([
            'title' => 'required|min:3|max:150'
        ]);
        $draft->title = $request->title;
        $draft->unsaved_changes = true;
        $draft->save();
        return [
            'message' => __('validation.draft.title.success-message')
        ];
    }

    public function updateText(Request $request, Draft $draft)
    {
        $this->authorize('update', $draft);
        $text = $request->get('text');
        $request->replace(['text' => strip_tags(str_replace('><', '> <', $text))]);
        $request->validate([
            'text' => 'required|min:150|max:8000'
        ]);
        $draft->text = $text;
        $draft->unsaved_changes = true;
        $draft->save();
        return [
            'message' => __('validation.draft.text.success-message')
        ];
    }

    public function uploadImage(Request $request, Draft $draft)
    {
        $this->authorize('update', $draft);
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:500',
        ]);
        $manager = new ImageManager(array('driver' => 'gd'));
        $order = $request->get('order');
        $draft->image_number_counter++;

        foreach ($request->file() as $file) {
            $file = $file;
        }

        $draftImage = new DraftImage;
        $draftImage->draft_id = $draft->id;
        $draftImage->order = $order;
        $draftImage->image_number = $draft->image_number_counter;
        $filename = $draft->id . "_$draft->image_number_counter." . $file->getClientOriginalExtension();
        $draftImage->filename = $filename;
        $draftImage->save();

        $image = $this->fitAndSaveImage($manager, $file, $filename, 'large', 800, 600);
        $this->resizeAndSaveImage($manager, $image, $filename, 'medium', 400, 300);
        $this->resizeAndSaveImage($manager, $image, $filename, 'small', 140, 105);

        $draft->unsaved_changes = true;
        $draft->save();

        return [
            'message' => __('validation.draft.upload-image.success-message'),
            'draftImages' => $this->mapImagePaths($draft)
        ];
    }

    private function fitAndSaveImage($manager, $file, $filename, $size, $width, $height)
    {
        $image = $manager->make($file)->fit($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $imageStream = $image->stream();
        Storage::disk('s3')->put("drafts/$size/$filename", $imageStream->__toString());
        return $image;
    }

    private function containAndSaveImage($manager, $file, $filename, $size, $width, $height)
    {
        $image = $manager->make($file)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->resizeCanvas($width, $height, 'center', false, '1b1b1b');
        $imageStream = $image->stream();
        Storage::disk('s3')->put("drafts/$size/$filename", $imageStream->__toString());
        return $image;
    }

    private function resizeAndSaveImage($manager, $image, $filename, $size, $width, $height): void
    {
        $image = $manager->make($image)->resize($width, $height);
        $imageStream = $image->stream();
        Storage::disk('s3')->put("drafts/$size/$filename", $imageStream->__toString());
    }

    public function reorderImages(Request $request, Draft $draft)
    {
        $this->authorize('update', $draft);
        $request->validate([
            'orders' => 'required|json',
        ]);
        $orders = json_decode($request->get('orders'));
        foreach ($orders as $index => $order) {
            $draft->draftImages()->where('image_number', $order->image_number)->update(['order' => $index]);
        }
        $draft->unsaved_changes = true;
        $draft->save();
        return [
            'message' => __('validation.draft.reorder-image.success-message'),
            'draftImages' => $this->mapImagePaths($draft)
        ];
    }

    public function deleteImage(Request $request, Draft $draft)
    {
        $this->authorize('update', $draft);
        // @todo validation here
        $image_number = $request->get('image_number');
        $draftImage = $draft->draftImages()->where('image_number', $image_number)->first();
        $draft->image_numbers_to_be_deleted_from_post = collect($draft->image_numbers_to_be_deleted_from_post)->merge($draftImage->image_number)->flatten();
        $draft->deleteImage($draftImage);
        $draft->unsaved_changes = true;
        $draft->save();
        return [
            'message' => __('validation.draft.delete-image.success-message'),
            'draftImages' => $this->mapImagePaths($draft)
        ];
    }

    private function mapImagePaths($draft)
    {
        $collection = collect($draft->draftImages)
            ->map(function ($draft_image) {
                if (Storage::disk('s3')->exists("drafts/large/" . $draft_image->filename)) {
                    $path = Storage::disk('s3')->url("drafts/large/$draft_image->filename");
                } else {
                    $path = '/images/image_not_available.png';
                }
                $draft_image->path = $path;
                return $draft_image;
            });
        return $collection->values();
    }

    public function updateVideoId(Request $request, Draft $draft)
    {
        $this->authorize('update', $draft);
        $request->validate([
            'type' => 'required|in:youtube,vimeo',
            'video_id' => 'required|min:1|max:15'
        ]);
        $type = $request->type;
        $video_id = $request->video_id;
        $draftVideo = $draft->draftVideos()->where('type', $type)->updateOrCreate(
            ['type' => $type],
            ['video_id' => $video_id]
        );
        $draft->unsaved_changes = true;
        $draft->save();
        return [
            'message' => __('validation.draft.video.success-message', ['type' => ucfirst($type)]),
            'draftVideo' => $draftVideo
        ];
    }

    public function publish(Draft $draft)
    {
        $this->authorize('publish', $draft);
        return $draft->publish();
    }

    public function cancelChanges(Draft $draft)
    {
        $this->authorize('update', $draft);
        return Post::find($draft->id)->copyPostToDraft($draft);
    }

    public function delete(Draft $draft)
    {
        $this->authorize('delete', $draft);
        return $draft->deleteDraftAndPost();
    }

}
