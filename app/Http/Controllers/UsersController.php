<?php

namespace App\Http\Controllers;

use App\Draft;
use App\Jobs\SelfDeleteUserAccount;
use App\Jobs\SendUserBannedEmail;
use App\Jobs\SendUserApplicationRejectedEmail;
use App\Jobs\SendUserApprovedEmail;
use App\Jobs\SendUserReactivatedEmail;
use App\Jobs\SendUserSuspendedEmail;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show', 'accountDeleted');
        $this->middleware('redirect_if_banned')->except('show');
        $this->middleware('needs_to_agree_to_policy_agreement')->except('show');
        $this->middleware('check_role:Admin', ['only' => [
            'manage',
            'approve',
            'reject',
            'suspend',
            'ban',
            'reactivate'
        ]]);
    }

    public function show(User $user)
    {
        if($user->is_banned_or_self_deactivated) {
            return abort(404);
        }
        $posts = Post::where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->paginate(12);
        return view('users.show', compact('user', 'posts'));
    }

    public function selfDeactivate(): void
    {
        $user = Auth::user();
        $user->self_deactivated_at = now();
        $user->save();
        $user->posts()->delete();
        $user->drafts()->delete();
        $user->posts()->withTrashed()->update(['self_deactivated' => true]);
        foreach($user->posts() as $post) {
            Cache::tags(["post_$post->id"])->flush();
        }
        Cache::tags(['categories', 'posts'])->flush();
    }

    public function selfReactivate(): void
    {
        $user = Auth::user();
        $user->self_deactivated_at = NULL;
        $user->save();
        $user->posts()->withoutGlobalScopes()->restore();
        $user->drafts()->withoutGlobalScopes()->restore();
        $user->posts()->withoutGlobalScopes()->update(['self_deactivated' => false]);
        foreach($user->posts() as $post) {
            Cache::tags(["post_$post->id"])->flush();
        }
        Cache::tags(['categories', 'posts'])->flush();
    }

    public function deleteMyAccount()
    {
        return view('dashboard.self_delete');
    }

    public function selfDelete()
    {
        dispatch(new SelfDeleteUserAccount(Auth::user()));
        Auth::logout();
        session(['can_view_account_deleted_page' => true]);
        return redirect()->route('users.accountDeleted');
    }

    public function accountDeleted()
    {
        if (session('can_view_account_deleted_page') != true) {
            return abort(403);
        }
        return view('users.account_deleted');
    }

    public function manage(User $user)
    {
        return view('users.manage', compact('user'));
    }

    public function approve(User $user)
    {
        $user->role = 'Author';
        $user->status = 'Active';
        $user->save();
        dispatch(new SendUserApprovedEmail($user));
        return back()->with('approved', __('validation.user.approve.success-message', ['public_name' => $user->public_name]));
    }

    public function reject(Request $request, User $user)
    {
        $message = $request->message;
        $user->status = 'Rejected';
        $user->save();
        dispatch(new SendUserApplicationRejectedEmail($user, $message));
        return back();
    }

    public function suspend(User $user)
    {
        $user->status = 'Suspended';
        $user->suspended_count = $user->suspended_count+1;
        $user->suspended_at = now();
        $user->save();
        dispatch(new SendUserSuspendedEmail($user));
        return back();
    }

    public function unsuspend(User $user)
    {
        $user->status = 'Active';
        $user->suspended_at = NULL;
        $user->save();
        dispatch(new SendUserReactivatedEmail($user));
        return back()->with('reactivated', __('validation.user.reactivate.success-message', ['public_name' => $user->public_name]));
    }

    public function ban(User $user)
    {
        $user->status = 'Banned';
        $user->banned_count = $user->banned_count+1;
        $user->save();
        $user->posts()->delete();
        $user->drafts()->delete();
        dispatch(new SendUserBannedEmail($user));
        foreach($user->posts() as $post) {
            Cache::tags(["post_$post->id"])->flush();
        }
        Cache::tags(['categories', 'posts'])->flush();
        return back();
    }

    public function unban(User $user)
    {
        $user->status = 'Active';
        $user->save();
        dispatch(new SendUserReactivatedEmail($user));
        $user->posts()->restore();
        $user->drafts()->restore();
        foreach($user->posts() as $post) {
            Cache::tags(["post_$post->id"])->flush();
        }
        Cache::tags(['categories', 'posts'])->flush();
        return back()->with('reactivated', __('validation.user.reactivate.success-message', ['public_name' => $user->public_name]));

    }

}
