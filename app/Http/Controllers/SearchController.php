<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function searchPage(Request $request)
    {
        $query = $request->get('query');
        $page = $request->get('page');
        $query = !$query ? "*" : $query;
        $page = !$page ? $page = 1 : $page;
        return view('posts.search', compact('posts', 'query', 'page'));
    }

    public function menuSearch($query)
    {
        if($query == "*") {
            $posts = Post::orderBy('created_at', 'desc')->paginate(6);
        } else {
            $posts = Post::search($query)->paginate(6);
        }
        foreach($posts as $post) {
            $post->publicationDate = $post->publication_date;
            $post->smallImage = $post->small_image;
            $post->title = $post->excerptTitle;
            $post->excerpt = $post->excerptText;
            $post->route = action('PostsController@show', ['category' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]);
        }
        return $posts;
    }

    public function searchPosts($query)
    {
        if($query == '*') {
            $posts = Post::orderBy('created_at', 'desc')->paginate(9);
        } else {
            $posts = Post::search($query)->paginate(9);
        }

        foreach($posts as $post) {
            $post->userPublicName = $post->user->public_name;
            $post->userUrl = action('UsersController@show', ['user' => $post->user]);
            $post->publicationDate = $post->publication_date;
            $post->mediumImage = $post->medium_image;
            $post->title = $post->excerptTitle;
            $post->excerpt = $post->excerptText;
            $post->route = action('PostsController@show', ['category' => $post->category->url, 'post_slug' => $post->slug, 'post' => $post->id]);
        }
        return $posts;
    }


}
