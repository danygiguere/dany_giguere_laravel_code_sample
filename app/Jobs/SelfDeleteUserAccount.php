<?php

namespace App\Jobs;

use App\Draft;
use App\Mail\UserAccountSelfDeletedEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

class SelfDeleteUserAccount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userPosts = $this->user->posts;
        foreach($userPosts as $post) {
            $post->deleteImagesFromS3();
            Draft::find($post->id)->deleteImagesFromS3();
            Cache::tags(["post_$post->id"])->flush();
        }
        $this->user->forceDelete();
        Cache::tags(['categories', 'posts'])->flush();

        // send confirmation email
        $email = new UserAccountSelfDeletedEmail($this->user);
        Mail::to($this->user->email)->send($email);
    }
}
