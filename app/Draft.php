<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class Draft extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'text', 'category_id', 'media_type', 'image'
    ];

    protected $casts = [
        'first_published_on' => 'dateTime',
        'image_numbers_to_be_deleted_from_post' => 'array',
    ];

    // for soft delete
    protected $dates = ['deleted_at'];

    /******** Relationships ********/
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function draftImages()
    {
        return $this->hasMany('App\DraftImage')->orderBy('order', 'asc');
    }

    public function draftVideos()
    {
        return $this->hasMany('App\DraftVideo');
    }

    /******** Accessors & Mutators ********/
    public function getExcerptTitleAttribute()
    {
        $excerptTitle = str_limit(strip_tags($this->title), 40, '...');
        return $excerptTitle;
    }

    public function getCreatedOnAttribute()
    {
        $createdOn = $this->created_at->diffInDays(\Carbon\Carbon::now()) >= 7 ? $this->created_at->format('M j, Y') : $this->created_at->diffForHumans();
        return $createdOn;
    }

    public function getPublicationDateAttribute()
    {
        if($this->first_published_on === NULL) {
            $publicationDate = 'Never Published';
        } else {
        $publicationDate = $this->first_published_on->diffInDays(\Carbon\Carbon::now()) >= 7 ? $this->first_published_on->format('M j, Y') : $this->first_published_on->diffForHumans();
        }
        return $publicationDate;
    }

//    @todo to be cached maybe ???
    public function getImageAttribute()
    {
        try{
            if ($this->media_type == "youtube" || $this->media_type == "vimeo") {
                $excerptImage = getMediaImageUrl($this->media_type, $this->draftVideos->where( 'type', $this->media_type )->first()->video_id);
            } elseif(Storage::disk('s3')->exists("drafts/large/".$this->draftImages->where('order', 0)->first()->filename)) {
                $excerptImage = Storage::disk('s3')->url("drafts/large/".$this->draftImages->where('order', 0)->first()->filename);
            } else {
                $excerptImage = '/images/image_not_available.png';
            }
        } catch(\Exception $e){
            $excerptImage = '/images/image_not_available.png';
        }
        return $excerptImage;
    }

    /******** Publishing Methods ********/
    public function publish()
    {
        $message = 'Nothing to publish';
        if ($this->unsaved_changes == true) {
            //if never published
            if (!$post = Post::find($this->id)) {
                $this->slug = str_slug($this->title, '-');
                $this->first_published_on = now();
                $this->save();
                $post = $this->copyTextsToPost();
            } else {
                $post->media_type = $this->media_type;
                $post->title = $this->title;
                $post->text = $this->text;
                $post->save();
            }

            if ($this->media_type == 'image') {
                $this->copyImagesToPost($post);
            } else {
                $this->copyVideoToPost($post);
            }
            $this->unsaved_changes = false;
            $this->save();
            Cache::tags(["post_$post->id", "posts", "categories"])->flush();
            $message = __('validation.draft.publish.success-message');
        }
        return [
            'message' => $message,
            'url' => action('PostsController@show', ['category_url' => $this->category->url, 'post_slug' => $this->slug, 'post' => $this->id])
        ];
    }

    public function copyTextsToPost()
    {
        return Post::create([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'media_type' => $this->media_type,
            'user_id' => $this->user_id,
            'slug' => $this->slug,
            'title' => $this->title,
            'text' => $this->text,
        ]);
    }

    public function copyImagesToPost($post): void
    {
        $this->deleteVideos();
        $post->deleteVideos();
        $post->removeDeletedDraftImagesFromPost($this);
        foreach ($this->draftImages as $draftImage) {
            if ($draftImage->copied_to_post == false) {
                Storage::disk('s3')->copy("drafts/large/$draftImage->filename", "posts/large/$draftImage->filename");
                Storage::disk('s3')->copy("drafts/medium/$draftImage->filename", "posts/medium/$draftImage->filename");
                Storage::disk('s3')->copy("drafts/small/$draftImage->filename", "posts/small/$draftImage->filename");

                $post->postImages()->create([
                    'image_number' => $draftImage->image_number,
                    'filename' => $draftImage->filename,
                    'order' => $draftImage->order
                ]);
                $draftImage->copied_to_post = true;
                $draftImage->save();
            } else {
                // else it means the user is not uploading a new image. So we'll only update the order column in case there has been a reorder.
                $post->postImages()->where('image_number', $draftImage->image_number)->update(['order' => $draftImage->order]);
            }
        }
    }

    public function copyVideoToPost($post): void
    {
        $this->deleteImages();
        $post->deleteImages();
        $draftVideo = $this->draftVideos()->where('type', $this->media_type)->first();
        $post->postVideos()->where('type', $this->media_type)->updateOrCreate([
            'type' => $draftVideo->type,
            'video_id' => $draftVideo->video_id
            ]);
    }

    /******** Delete Draft Methods ********/
    public function deleteDraftAndPost()
    {
        // images and videos rows are deleted on cascade when deleting drafts and posts
        $post = Post::find($this->id);
        $this->deleteImagesFromS3();
        $this->forceDelete();
        $post->deleteImagesFromS3();
        $post->forceDelete();
        Cache::tags(['categories', 'posts'])->flush();
        return ['message' => __('validation.draft.delete.success-message')];
    }

    /******** Delete Images Methods ********/
    public function deleteImageFromS3($filename)
    {
        Storage::disk('s3')->delete("drafts/large/$filename");
        Storage::disk('s3')->delete("drafts/medium/$filename");
        Storage::disk('s3')->delete("drafts/small/$filename");
    }

    public function deleteImagesFromS3()
    {
        foreach ($this->draftImages as $draftImage) {
            $this->deleteImageFromS3($draftImage->filename);
        }
    }

    public function deleteImage($draftImage)
    {
        $this->deleteImageFromS3($draftImage->filename);
        $draftImage->forceDelete();
    }

    public function deleteImages()
    {
        foreach ($this->draftImages as $draftImage) {
            $this->deleteImage($draftImage);
        }
    }

    /******** Delete Videos Methods ********/
    public function deleteVideos()
    {
        foreach($this->draftVideos as $draftVideo) {
            $draftVideo->forceDelete();
        }
    }

}
