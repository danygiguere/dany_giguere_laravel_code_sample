<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $fillable = [
        'name', 'url'
    ];

    public function getRouteKeyName() {
        return 'url';
    }

    /******** Relationships ********/
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    /******** Methods with Redis caching ********/
    public static function hasPosts($categoryUrl)
    {
        return Cache::tags(['categories'])->rememberForever($categoryUrl.'_has_posts', function () use ($categoryUrl) {
            if($category = Category::where('url', $categoryUrl)->first()) {
                return count($category->posts) > 0;
            }
            return false;
        });
    }

    public static function getAllAttribute()
    {
        return Cache::tags(['categories'])->rememberForever('all_categories', function () {
            return DB::table('categories')->select('id', 'name')->get();
        });
    }

    // get the categories ordered by most popular. Calculated from the date of the 10th post from the most popular category
    public static function orderedByTrending()
    {
        return Cache::tags(['categories'])->remember('categories_ordered_by_trending', config('app.redis_caching_time_long'), function () {
            $categoriesFromPosts = DB::table('posts')
                ->join('categories', 'categories.id', '=', 'posts.category_id')
                ->leftJoin('views', 'views.post_id', '=', 'posts.id')
                ->select('posts.category_id as id', 'categories.name', 'categories.url', DB::raw('IFNULL(SUM(views.total), 0) as total_views'))
                ->where('posts.created_at', '>=', Post::getDateOfThirtiethPost())
                ->groupBy('posts.category_id')
                ->orderBy('total_views', 'desc')
                ->get();

            $allActiveCategories = DB::table('categories')
                ->join('posts', 'posts.category_id', '=', 'categories.id')
                ->select('categories.id', 'categories.name', 'categories.url')
                ->groupBy('categories.id')
                ->get();

            $categoriesOrderedByTrending = $categoriesFromPosts->merge($allActiveCategories)->groupBy('id');
            $categories = $categoriesOrderedByTrending->map(function ($item) {
                return $item->first();
            });
            return $categories;
        });
    }

}
