<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

// //route to test email. Enter the user id.
//Route::get('/mail', function() {
//    $user = \App\User::find(10);
//    return new \App\Mail\UserApprovedEmail($user);
//});

Auth::routes();

Route::get('/', 'PagesController@home');
Route::get('/policy-agreement', 'PagesController@policyAgreement')->name('policyAgreement');
Route::get('/agree-to-policy-agreement', 'PagesController@agreeToPolicyAgreement')->name('agreeToPolicyAgreement');
Route::get('/privacy', 'PagesController@privacy')->name('privacy');
Route::get('/terms', 'PagesController@terms')->name('terms');
Route::get('/guidelines', 'PagesController@guidelines')->name('guidelines');
Route::get('/search/{query?}/{page?}', 'SearchController@searchPage')->name('searchPage');
Route::get('/search_posts/{query}', 'SearchController@searchPosts');
Route::get('/menu_search/{query}', 'SearchController@menuSearch');

Route::get('/verification-email-sent', 'VerificationController@verificationEmailSent')->name('verify.verificationEmailSent');
Route::get('/verification-email-resent', 'VerificationController@verificationEmailResent')->name('verify.verificationEmailResent');
Route::get('/verify-email/{verification_token}', 'VerificationController@verifyEmail')->name('verify.verifyEmail');
Route::get('/email-verified', 'VerificationController@emailVerified')->name('verify.emailVerified');
Route::get('/email-not-verified/{auth_token}', 'VerificationController@emailNotVerified')->name('verify.emailNotVerified');
Route::get('/resend-verification-email/{auth_token}', 'VerificationController@resendVerificationEmail')->name('verify.resendVerificationEmail');
Route::get('/banned-user', 'VerificationController@bannedUser')->name('verify.bannedUser');

Route::get('/dashboard', 'DashboardController@summary')->name('dashboard');
Route::get('/dashboard/posts', 'DashboardController@posts')->name('dashboard.posts');
Route::get('/dashboard/views', 'DashboardController@views')->name('dashboard.views');
Route::get('/dashboard/profile', 'DashboardController@profile')->name('dashboard.profile');
Route::get('/dashboard/payments', 'DashboardController@payments')->name('dashboard.payments');
Route::get('/dashboard/users-management', 'DashboardController@usersManagement')->name('dashboard.usersManagement');
Route::get('/dashboard/posts-management', 'DashboardController@postsManagement')->name('dashboard.postsManagement');
Route::get('/dashboard/categories', 'DashboardController@categories')->name('dashboard.categories');
Route::post('/dashboard/categories', 'DashboardController@storeCategories')->name('dashboard.storeCategories');
Route::get('/dashboard/settings', 'DashboardController@settings')->name('dashboard.settings');

Route::put('/dashboard/update-profile', 'DashboardController@updateProfile');
Route::put('/dashboard/save-profile-image', 'DashboardController@saveProfileImage');
Route::put('/dashboard/update-personal-info', 'DashboardController@updatePersonalInfo');
Route::put('/dashboard/update-password', 'DashboardController@updatePassword');
Route::get('/dashboard/delete-my-account', 'UsersController@deleteMyAccount')->name('users.deleteMyAccount');
Route::get('/dashboard/search_posts', 'DashboardController@searchPosts');
Route::get('/dashboard/search_posts_for_all_users', 'DashboardController@searchPostsForAllUsers');
Route::get('/dashboard/search_users', 'DashboardController@searchUsers');
Route::get('/dashboard/search_views', 'DashboardController@searchViews');

Route::get('/users/{user}/manage', 'UsersController@manage')->name('users.manage');
Route::get('/users/{user}/approve', 'UsersController@approve')->name('users.approve');
Route::put('/users/{user}/reject', 'UsersController@reject')->name('users.reject');
Route::get('/users/{user}/suspend', 'UsersController@suspend')->name('users.suspend');
Route::get('/users/{user}/unsuspend', 'UsersController@unsuspend')->name('users.unsuspend');
Route::get('/users/{user}/ban', 'UsersController@ban')->name('users.ban');
Route::get('/users/{user}/unban', 'UsersController@unban')->name('users.unban');
Route::put('/users/self-deactivate', 'UsersController@selfDeactivate')->name('users.selfDeactivate');
Route::put('/users/self-reactivate', 'UsersController@selfReactivate')->name('users.selfReactivate');
Route::put('/users/self-delete', 'UsersController@selfDelete')->name('users.selfDelete');
Route::get('/users/account-deleted', 'UsersController@accountDeleted')->name('users.accountDeleted');
Route::get('/users/{user}', 'UsersController@show')->name('users.show');


Route::get('/posts/{draft}/edit', 'DraftsController@edit')->name('drafts.edit');
Route::put('/drafts/{draft}/update-category', 'DraftsController@updateCategory');
Route::put('/drafts/{draft}/update-media-type', 'DraftsController@updateMediaType');
Route::put('/drafts/{draft}/update-title', 'DraftsController@updateTitle');
Route::put('/drafts/{draft}/update-text', 'DraftsController@updateText');
Route::put('/drafts/{draft}/upload-image', 'DraftsController@uploadImage');
Route::put('/drafts/{draft}/reorder-images', 'DraftsController@reorderImages');
Route::delete('/drafts/{draft}/delete-image', 'DraftsController@deleteImage');
Route::put('/drafts/{draft}/update-video-id', 'DraftsController@updateVideoId');
Route::put('/drafts/{draft}/publish', 'DraftsController@publish');
Route::put('/drafts/{draft}/cancel-changes', 'DraftsController@cancelChanges');
Route::put('/drafts/{draft}/delete', 'DraftsController@delete');

Route::get('/posts/create', 'DraftsController@create')->name('drafts.create');
Route::put('/posts/toggle-remove', 'PostsController@toggleRemove')->name('posts.toggleRemove');

Route::get('/{category}/{post_slug}/{post_id}', 'PostsController@show')->name('posts.show');
// *** important *** this route should be last for performance reasons
Route::get('/{category}', 'PagesController@category')->name('category');

