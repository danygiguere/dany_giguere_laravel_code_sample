<?php

namespace Tests\Feature;

use App\Draft;
use App\Post;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReactivatedUserTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $reactivatedUser;
    protected $userReactivated;
    protected $author;
    protected $draft;
    protected $postUrl;
    protected $newDraftUrl;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'name' => 'johndoe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret'),
            'self_deactivated_at' => now()
        ]);
        $this->author = $this->actingAs($this->user);
        $this->draft = factory(Draft::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1,
            'unsaved_changes' => 1
        ]);
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1,
            'self_deactivated' => 1
        ]);
        $nextDraftId = $this->draft->id + 1;
        $this->newDraftUrl = '/posts/'.$nextDraftId.'/edit';
        $this->postUrl = '/'. $post->category->url . '/' . $post->slug . '/' . $post->id;
    }

    /** @test */
    public function user_can_reactivate_his_account()
    {
        $response = $this->author->json('PUT', '/users/self-reactivate', collect($this->user)->toArray());
        $response->assertStatus(200);
    }

    /** @test */
    public function reactivated_author_can_access_his_profile_page()
    {
        $this->author->json('PUT', '/users/self-reactivate', collect($this->user)->toArray());
        $this->author->get('dashboard/profile')->assertStatus(200);
    }

    /** @test */
    public function reactivated_author_can_access_dashboard_posts_page()
    {
        $this->author->json('PUT', '/users/self-reactivate', collect($this->user)->toArray());
        $this->author->get('dashboard/posts')->assertStatus(200);
    }

    /** @test */
    public function reactivated_author_can_create_posts()
    {
        $this->author->json('PUT', '/users/self-reactivate', collect($this->user)->toArray());
        $this->author->get('/posts/create')->assertRedirect($this->newDraftUrl);

    }

    /** @test */
    public function reactivated_author_can_edit_posts()
    {
        $this->author->json('PUT', '/users/self-reactivate', collect($this->user)->toArray());
        $this->author->get('/posts/'.$this->draft->id.'/edit')->assertStatus(200);
    }

    /** @test */
    public function visitor_can_see_reactivated_user_profile()
    {
        $this->actingAs($this->user)->json('PUT', '/users/self-reactivate', collect($this->user)->toArray());
        $this->get('/users/'.$this->user->url)->assertStatus(200);
    }

    /** @test */
    public function visitor_can_see_reactivated_user_posts()
    {
        $this->actingAs($this->user)->json('PUT', '/users/self-reactivate', collect($this->user)->toArray());
        $this->get($this->postUrl)->assertStatus(200);
    }
}
