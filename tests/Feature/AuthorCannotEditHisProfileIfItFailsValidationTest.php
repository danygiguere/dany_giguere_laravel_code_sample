<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class AuthorCannotEditHisProfileIfItFailsValidationTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function author_cannot_update_his_profile_with_a_blank_public_name()
    {
        $user = factory(User::class)->create([
            'public_name' => '',
        ]);
        $response = $this->actingAs($user)->json('PUT', '/dashboard/update-profile/', collect($user)->toArray());
        $response
            ->assertStatus(422)
            ->assertJson([
                'errors' => ['public_name' => [
                        0 => __('validation.required', ['attribute' => 'public name'])
                    ]
                ]
            ]);
    }

    /** @test */
    public function author_cannot_update_his_profile_with_a_blank_url()
    {
        $user = factory(User::class)->create([
            'url' => '',
        ]);
        $response = $this->actingAs($user)->json('PUT', '/dashboard/update-profile/', collect($user)->toArray());
        $response
            ->assertStatus(422)
            ->assertJson([
                'errors' => ['url' => [
                        0 => __('validation.required', ['attribute' => 'url'])
                    ]
                ]
            ]);
    }

    /** @test */
    public function author_cannot_update_profile_image_if_not_of_right_type()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json('PUT', '/dashboard/save-profile-image/', ['image' => UploadedFile::fake()->create('virus.dmg', 100)]);
        $response
            ->assertStatus(422)
            ->assertJson([
                'errors' => ['image' => [
                        0 => __('validation.image', ['attribute' => 'image']),
                        1 => __('validation.mimes', ['attribute' => 'image', 'values' => 'jpeg, png, jpg, gif, svg'])
                    ]
                ]
            ]);
    }

    /** @test */
    public function author_cannot_update_profile_image_if_too_big()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json('PUT', '/dashboard/save-profile-image/', ['image' => UploadedFile::fake()->create('mock.jpg', 10000)]);
        $response
            ->assertStatus(422)
            ->assertJson([
                'errors' => ['image' => [
                        0 => __('validation.max.file', ['attribute' => 'image', 'max' => '4000']),
                    ]
                ]
            ]);
    }

}
