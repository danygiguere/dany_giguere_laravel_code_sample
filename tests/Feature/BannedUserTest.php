<?php

namespace Tests\Feature;

use App\Post;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BannedUserTest extends TestCase
{
    use DatabaseTransactions;

    protected $superadmin;
    protected $bannedUser;
    protected $user;
    protected $postUrl;

    public function setUp()
    {
        parent::setUp();

        $this->superadmin = factory(User::class)->create([
            'role' => 'Superadmin',
        ]);
        $this->bannedUser = factory(User::class)->create([
            'status' => 'Banned'
        ]);
        $this->user = factory(User::class)->create();

        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1,
        ]);
        $this->postUrl = '/'. $post->category->url . '/' . $post->slug . '/' . $post->id;
    }

    /** @test */
    public function banned_user_is_logged_out_and_redirected_to_banned_user_page()
    {
        $response = $this->actingAs($this->bannedUser)->get('/dashboard/');
        $this->assertGuest($guard = null);
        $response->assertRedirect('/banned-user');
    }

    /** @test */
    public function visitor_cannot_see_banned_user_profile() {
        $this->get('/users/'.$this->bannedUser->url)->assertStatus(404);
    }

    /** @test */
    public function visitor_cannot_see_banned_user_posts()
    {
        $this->actingAs($this->superadmin)->get("/users/".$this->user->name."/ban");
        $this->get($this->postUrl)->assertStatus(404);
    }

}
