<?php

namespace Tests\Feature;

use App\PolicyAgreement;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PolicyAgreementTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'last_agreed_policy' => 0
        ]);
    }

    /** @test */
    public function user_needs_to_agree_to_policy_agreement()
    {
        $response = $this->actingAs($this->user)->get('/dashboard');
        $response->assertRedirect('/policy-agreement');
    }

    /** @test */
    public function user_can_agree_to_policy_agreement()
    {
        $response = $this->actingAs($this->user)->get('/agree-to-policy-agreement');
        $response->assertRedirect('/dashboard');
        $this->assertDatabaseHas('policy_agreements', [
            'user_id' => $this->user->id
        ]);
        $this->assertDatabaseHas('users', [
            'last_agreed_policy' => PolicyAgreement::currentPolicyNumber()
        ]);
    }
}
