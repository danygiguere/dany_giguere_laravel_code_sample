<?php

namespace Tests\Feature;

use App\Jobs\SendUserApprovedEmail;
use App\Jobs\SendUserSignedUpEmail;
use App\Jobs\SendUserVerifyEmail;
use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Queue;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrationProcessTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $otherUser;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'role' => 'Unverified',
            'status' => 'Applicant',
            'verification_token_created_at' => today()
        ]);

        $this->otherUser = factory(User::class)->create([
            'name' => 'janedoe',
            'email' => 'janedoe@test.com',
            'password' => bcrypt('secret'),
            'role' => 'Unverified',
            'status' => 'Applicant',
            'verification_token_created_at' => today()
        ]);
    }

    /** @test */
    public function visitor_can_register()
    {
        Queue::fake();
        $response = $this->post('/register', [
            'email' => 'johndoe@test.com',
            'password' => 'secret',
            'password_confirmation' => 'secret',
            'name' => 'johndoe',
            'public_name' => 'Johndoe',
            'url' => 'johndoe',
            'application_text' => 'some text'
        ]);
        $response->assertRedirect('/verification-email-sent');
        Queue::assertPushed(SendUserVerifyEmail::class, 1);
    }

    /** @test */
    public function unverified_user_cannot_login()
    {
        $response = $this->post('/login', [
            'email' => $this->user->email,
            'password' => 'secret',
        ]);
        $response->assertRedirect("/email-not-verified/".$this->user->auth_token);
    }

    /** @test */
    public function unverified_user_can_verify_email()
    {
        Queue::fake();
        $response = $this->get("/verify-email/".$this->user->verification_token);
        $response->assertRedirect('/email-verified');
        $this->assertDatabaseHas('users', [
            'email' => $this->user->email,
            'role' => 'Applicant'
        ]);
        Queue::assertPushed(SendUserSignedUpEmail::class, 1);
    }

    /** @test */
    public function applicant_can_have_verification_email_resent()
    {
        Queue::fake();
        $response = $this->actingAs($this->otherUser)->get('/resend-verification-email/'.$this->otherUser->auth_token);
        $response->assertRedirect('/verification-email-resent');
        Queue::assertPushed(SendUserVerifyEmail::class, 1);
    }

    /** @test */
    public function superadmin_can_approve_applicant()
    {
        Queue::fake();
        $user = factory(User::class)->create([
            'role' => 'Superadmin'
        ]);
        $superadmin = $this->actingAs($user);
        $superadmin->get("/users/".$this->user->name."/approve");
        $this->assertDatabaseHas('users', [
            'email' => $this->user->email,
            'role' => 'Author',
            'status' => 'Active'
        ]);
        Queue::assertPushed(SendUserApprovedEmail::class, 1);
    }

    /** @test */
    public function approved_user_can_login_as_author()
    {
        $user = factory(User::class)->create([
            'role' => 'Author',
            'status' => 'Active'
        ]);
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'secret',
        ]);
        $response->assertRedirect('/dashboard');
    }

}
