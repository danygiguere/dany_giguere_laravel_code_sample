<?php

namespace Tests\Feature;

use App\Draft;
use App\Post;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeactivatedUserTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $author;
    protected $draft;
    protected $postUrl;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'name' => 'johndoe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        $this->author = $this->actingAs($this->user);
        $this->draft = factory(Draft::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1,
            'unsaved_changes' => 1
        ]);
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1,
        ]);
        $this->postUrl = '/'. $post->category->url . '/' . $post->slug . '/' . $post->id;
    }

    /** @test */
    public function user_can_deactivate_his_account()
    {
        $response = $this->author->json('PUT', '/users/self-deactivate', collect($this->user)->toArray());
        $response->assertStatus(200);
    }

    /** @test */
    public function deactivated_author_cannot_access_his_profile_page()
    {
        $this->author->json('PUT', '/users/self-deactivate', collect($this->user)->toArray());
        $this->author->get('dashboard/profile')->assertStatus(403);
    }

    /** @test */
    public function deactivated_author_cannot_access_dashboard_posts_page()
    {
        $this->author->json('PUT', '/users/self-deactivate', collect($this->user)->toArray());
        $this->author->get('dashboard/posts')->assertStatus(403);
    }

    /** @test */
    public function deactivated_author_cannot_create_posts()
    {
        $this->author->json('PUT', '/users/self-deactivate', collect($this->user)->toArray());
        $this->author->get('/posts/create')->assertStatus(403);
    }

    /** @test */
    public function deactivated_author_cannot_edit_posts()
    {
        $this->author->json('PUT', '/users/self-deactivate', collect($this->user)->toArray());
        $this->author->get('/posts/'.$this->draft->id.'/edit')->assertStatus(404);
    }

    /** @test */
    public function visitor_cannot_see_deactivated_user_profile()
    {
        $this->actingAs($this->user)->json('PUT', '/users/self-deactivate', collect($this->user)->toArray());
        $this->get('/users/'.$this->user->url)->assertStatus(404);
    }

    /** @test */
    public function visitor_cannot_see_deactivated_user_posts()
    {
        $this->actingAs($this->user)->json('PUT', '/users/self-deactivate', collect($this->user)->toArray());
        $this->get($this->postUrl)->assertStatus(404);
    }

}
