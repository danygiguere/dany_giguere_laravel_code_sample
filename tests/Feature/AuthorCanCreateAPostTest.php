<?php

namespace Tests\Feature;

use App\Draft;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class AuthorCanCreateAPostTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $post;
    protected $draft;
    protected $author;
    protected $newDraftUrl;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'name' => 'johndoe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret')
        ]);
        $this->draft = factory(Draft::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1
        ]);
        $nextDraftId = $this->draft->id + 1;
        $this->newDraftUrl = '/posts/'.$nextDraftId.'/edit';
        $this->author = $this->actingAs($this->user);
    }

    /** @test */
    public function author_can_visit_the_create_draft_page()
    {
        $this->author->get('/posts/create')->assertRedirect($this->newDraftUrl);
        $this->author->get($this->newDraftUrl)->assertStatus(200);
    }

    /** @test */
    public function author_can_upload_and_delete_a_draft_image()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->draft->id.'/upload-image/', ['image' => UploadedFile::fake()->image('mock.jpg')]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.draft.upload-image.success-message')
            ]);
        $response = $this->author->json('DELETE', '/drafts/'.$this->draft->id.'/delete-image/', ['image_number' => 1]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.draft.delete-image.success-message')
            ]);
    }

//    /** @test */
//    public function author_can_reorder_draft_images()
//    {
//        // @todo
//
//    }

    /** @test */
    public function author_can_update_draft_category()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->draft->id.'/update-category/', ['category_id' => 1]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.draft.category.success-message')
            ]);
    }

    /** @test */
    public function author_can_update_draft_media_type()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->draft->id.'/update-media-type/', ['media_type' => 'image']);
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.draft.media-type.success-message')
            ]);
    }

    /** @test */
    public function author_can_update_draft_title()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->draft->id.'/update-title/', ['title' => 'test']);
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.draft.title.success-message')
            ]);
    }

    /** @test */
    public function author_can_update_draft_text()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->draft->id.'/update-text/', ['text' => 'test']);
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.draft.text.success-message')
            ]);
    }

    /** @test */
    public function author_can_add_youtube_video()
    {
        $this->author->json('PUT', '/drafts/'.$this->draft->id.'/update-media-type/', ['media_type' => 'youtube']);
        $response = $this->author->json('PUT', '/drafts/'.$this->draft->id.'/update-video-id/', ['type' => 'youtube', 'video_id' => 'vtxou4LTQS8']);
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.draft.video.success-message', ['type' => 'Youtube'])
            ]);
    }

}


