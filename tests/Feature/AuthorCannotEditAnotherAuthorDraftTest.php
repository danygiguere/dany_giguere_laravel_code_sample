<?php

namespace Tests\Feature;

use App\Draft;
use App\Post;
use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthorCannotEditAnotherAuthorDraftTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $otherUser;
    protected $post;
    protected $otherUserPost;
    protected $draft;
    protected $otherUserDraft;
    protected $author;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'name' => 'johndoe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret')
        ]);
        $this->otherUser = factory(User::class)->create([
            'name' => 'joeblack',
            'email' => 'joeblack@gmail.com',
            'password' => bcrypt('secret')
        ]);
        $this->draft = factory(Draft::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1
        ]);
        $this->post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1
        ]);
        $this->otherUserDraft = factory(Draft::class)->create([
            'user_id' => $this->otherUser->id,
            'category_id' => 1
        ]);
        $this->otherUserPost = factory(Post::class)->create([
            'user_id' => $this->otherUser->id,
            'category_id' => 1
        ]);
        $this->author = $this->actingAs($this->user);
    }

    /** @test */
    public function author_cannot_visit_an_edit_post_page_that_belongs_to_another_user()
    {
        $this->author->get('/posts/'.$this->otherUserDraft->id.'/edit')->assertStatus(403);
    }

    /** @test */
    public function author_cannot_update_draft_category_that_belongs_to_another_user()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->otherUserDraft->id.'/update-category/', ['category_id' => 1]);
        $response->assertStatus(403);
    }

    /** @test */
    public function author_cannot_update_draft_media_type_that_belongs_to_another_user()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->otherUserDraft->id.'/update-media-type/', ['media_type' => 'image']);
        $response->assertStatus(403);
    }

    /** @test */
    public function author_cannot_update_draft_title_that_belongs_to_another_user()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->otherUserDraft->id.'/update-title/', ['title' => 'test']);
        $response->assertStatus(403);
    }

    /** @test */
    public function author_cannot_update_draft_text_that_belongs_to_another_user()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->otherUserDraft->id.'/update-text/', ['text' => 'test']);
        $response->assertStatus(403);
    }

    /** @test */
    public function author_cannot_upload_a_draft_image_that_belongs_to_another_user()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->otherUserDraft->id.'/upload-image/', ['image' => UploadedFile::fake()->image('mock.jpg')]);
        $response->assertStatus(403);
    }

    /** @test */
    public function author_cannot_delete_a_draft_image_that_belongs_to_another_user()
    {
        $response = $this->author->json('DELETE', '/drafts/'.$this->otherUserDraft->id.'/delete-image/', ['image_number' => 1]);
        $response->assertStatus(403);
    }
}


