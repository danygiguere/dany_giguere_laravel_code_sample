<?php

namespace Tests\Feature;

use App\Jobs\SelfDeleteUserAccount;
use App\Post;
use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Queue;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SelfDeletedUserTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $draft;
    protected $postUrl;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'name' => 'johndoe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret')
        ]);
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1,
        ]);
        $this->postUrl = '/'. $post->category->url . '/' . $post->slug . '/' . $post->id;
    }

    /** @test */
    public function user_can_permanently_delete_his_account()
    {
        Queue::fake();
        $response = $this->actingAs($this->user)->json('PUT', '/users/self-delete', collect($this->user)->toArray());
        Queue::assertPushed(SelfDeleteUserAccount::class, 1);
        $response
            ->assertStatus(302)
            ->assertRedirect('/users/account-deleted');
    }

}
