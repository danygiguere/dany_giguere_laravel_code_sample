<?php

namespace Tests\Feature;

use App\Draft;
use App\Post;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AuthorCanPerformActionsOnHisPostTest extends TestCase
{
    use DatabaseTransactions;

    protected $author;
    protected $draft;
    protected $post;
    protected $postUrl;

    public function setUp()
    {
        parent::setUp();

        $user = factory(User::class)->create([
            'name' => 'johndoe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret')
        ]);
        $this->draft = factory(Draft::class)->create([
            'user_id' => $user->id,
            'category_id' => 1,
            'unsaved_changes' => 1
        ]);
        $this->post = factory(Post::class)->create([
            'id' => $this->draft->id,
            'user_id' => $user->id,
            'category_id' => 1,
        ]);
        $this->author = $this->actingAs($user);
        $this->postUrl = '/'. $this->draft->category->url . '/' . $this->draft->slug . '/' . $this->draft->id;
    }

    /** @test */
    public function author_can_publish_draft()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->draft->id.'/publish');
        $response->assertStatus(200)
        ->assertJson([
            'message' => __('validation.draft.publish.success-message')
        ]);
        $this->assertDatabaseHas('posts', [
            'title' => $this->draft->title,
            'text' => $this->draft->text
        ]);
    }

    /** @test */
    public function author_can_cancel_changes()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->draft->id.'/cancel-changes');
        $response->assertStatus(200)
            ->assertJson([
                'message' => __('validation.draft.cancel-changes.success-message')
            ]);
    }

    /** @test */
    public function author_can_delete_draft()
    {
        $response = $this->author->json('PUT', '/drafts/'.$this->draft->id.'/delete');
        $response->assertStatus(200)
            ->assertJson([
                'message' => __('validation.draft.delete.success-message')
            ]);
        $this->get($this->postUrl)->assertStatus(404);
    }
}
