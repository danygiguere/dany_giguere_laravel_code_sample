<?php

namespace Tests\Feature;

use App\Draft;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\User;

class SuspendedUserTest extends TestCase
{
    use DatabaseTransactions;

    protected $superadmin;
    protected $suspendedUser;
    protected $draft;
    protected $postUrl;

    public function setUp()
    {
        parent::setUp();

        $this->superadmin = factory(User::class)->create([
            'role' => 'Superadmin',
        ]);
        $this->suspendedUser = factory(User::class)->create([
            'status' => 'Suspended'
        ]);
        $this->user = factory(User::class)->create();
        $this->draft = factory(Draft::class)->create([
            'user_id' => $this->suspendedUser->id,
            'category_id' => 1,
        ]);
    }

    /** @test */
    public function suspended_user_cannot_create_new_post()
    {
        $response = $this->actingAs($this->suspendedUser)->get("/posts/create");
        $response->assertStatus(403);
    }

    /** @test */
    public function suspended_user_can_edit_post()
    {
        $response = $this->actingAs($this->suspendedUser)->get("/posts/". $this->draft->id ."/edit");
        $response->assertStatus(200);
    }
}
