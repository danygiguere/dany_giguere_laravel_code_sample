<?php

namespace Tests\Feature;

use App\Post;
use App\PostImage;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class VisitorTest extends TestCase
{
    use DatabaseTransactions;

    protected $post;
    protected $user;
    protected $postUrl;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'name' => 'johndoe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret')
        ]);
        $this->post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1
        ]);
        factory(PostImage::class)->create([
            'post_id' => $this->post->id
        ]);

        $this->postUrl = '/'. $this->post->category->url . '/' . $this->post->slug . '/' . $this->post->id;
    }

    /** @test */
    public function visitor_can_visit_all_public_pages()
    {
        $this->get('/')->assertStatus(200);
        $this->get($this->post->category->url)->assertStatus(200);
        $this->get('/register')->assertStatus(200);
        $this->get('/login')->assertStatus(200);
        $this->get('/search')->assertStatus(200);
        $this->get($this->postUrl)->assertStatus(200);
        $this->get('/users/'. $this->post->user->url)->assertStatus(200);
        $this->get('/privacy')->assertStatus(200);
        $this->get('/terms')->assertStatus(200);
        $this->get('/guidelines')->assertStatus(200);
    }

    /** @test */
    public function visitor_can_see_a_post_content()
    {
        $this->get($this->postUrl)->assertSeeText($this->post->title);
        $this->get($this->postUrl)->assertSeeText($this->post->text);
    }

    /** @test */
    public function visitor_has_cookie_set_to_visited_after_visiting_post()
    {
        $this->get($this->postUrl)->assertCookie('post-' . $this->post->id, $value = 'visited');
    }

    /** @test */
    public function visitor_can_see_most_popular_post_on_home_and_category_pages()
    {
        $mostPopularPost = collect(Post::trending())->first();
        $this->get('/')->assertSee($mostPopularPost->excerpt_title);
        $mostPopularPostForCategory = collect(Post::trendingForCategory(1))->first();
        $this->get($this->post->category->url)->assertSee($mostPopularPostForCategory->excerpt_title);
    }

    /** @test */
    public function visitor_cannot_visit_author_nor_superadmin_pages()
    {
        $this->get('/dashboard')->assertRedirect('/login');
        $this->get('/dashboard/posts')->assertRedirect('/login');
        $this->get('/dashboard/views')->assertRedirect('/login');
        $this->get('/dashboard/profile')->assertRedirect('/login');
        $this->get('/dashboard/payments')->assertRedirect('/login');
        $this->get('/dashboard/settings')->assertRedirect('/login');
        $this->get('/dashboard/users-management')->assertRedirect('/login');
        $this->get('/users/johndoe/manage')->assertRedirect('/login');
        $this->get('/dashboard/posts-management')->assertRedirect('/login');
        $this->get('/dashboard/categories')->assertRedirect('/login');
        $this->get('/posts/1/edit')->assertStatus(403);
    }

    /** @test */
    public function visitor_can_see_404()
    {
        $this->get('/whateverpage')->assertStatus(404);
        $this->get('/whateverpage/otherparam')->assertStatus(404);
    }

}
