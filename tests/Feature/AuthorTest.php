<?php

namespace Tests\Feature;

use App\Draft;
use App\DraftImage;
use App\Post;
use App\PostImage;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthorTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $post;
    protected $draft;
    protected $author;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'name' => 'johndoe',
            'url' => 'johndoe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret')
        ]);
        $this->draft = factory(Draft::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1
        ]);
        $this->post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1
        ]);
        factory(PostImage::class)->create([
            'post_id' => $this->post->id
        ]);
        factory(DraftImage::class)->create([
            'draft_id' => $this->draft->id
        ]);
        $this->author = $this->actingAs($this->user);
    }

    /** @test */
    public function author_can_visit_all_dashboard_pages()
    {
        $this->author->get('/dashboard')->assertStatus(200);
        $this->author->get('/dashboard/posts')->assertStatus(200);
        $this->author->get('/dashboard/views')->assertStatus(200);
        $this->author->get('/dashboard/profile')->assertStatus(200);
        $this->author->get('/dashboard/payments')->assertStatus(200);
        $this->author->get('/dashboard/settings')->assertStatus(200);
        $this->author->get('/posts/'.$this->draft->id.'/edit')->assertStatus(200);
    }

    /** @test */
    public function author_cannot_visit_admin_nor_superadmin_pages()
    {
        $this->author->get('/dashboard/users-management')->assertStatus(403);
        $this->author->get('/users/'.$this->user->url.'/manage')->assertStatus(403);
        $this->author->get('/dashboard/posts-management')->assertStatus(403);
        $this->author->get('/dashboard/categories')->assertStatus(403);
    }

    /** @test */
    public function author_cannot_perform_admin_nor_superadmin_actions()
    {
        $this->author->get("/users/".$this->user->url."/approve")->assertStatus(403);
    }

}


