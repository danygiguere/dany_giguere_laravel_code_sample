<?php

namespace Tests\Feature;

use App\PolicyAgreement;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MiddlewaresTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function user_has_role_or_higher()
    {
        $user = factory(User::class)->create([
            'role' => 'Author'
        ]);
        $response = $user->hasRoleOrHigher('Admin');
        $this->assertFalse($response);
    }
}
