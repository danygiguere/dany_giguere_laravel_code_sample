<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AuthorCanEditHisProfileTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $author;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'name' => 'johndoe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret')
        ]);
        $this->author = $this->actingAs($this->user);
    }

    /** @test */
    public function author_can_update_his_profile()
    {
        $response = $this->author->json('PUT', '/dashboard/update-profile/', collect($this->user)->toArray());
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.dashboard.profile.success-message')
            ]);
    }

    /** @test */
    public function author_can_upload_profile_image()
    {
        $response = $this->author->json('PUT', '/dashboard/save-profile-image/', ['image' => UploadedFile::fake()->image('mock.jpg')]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.dashboard.profile-image.success-message')
            ]);
        Storage::disk('s3')->delete($this->user->image);
    }

}


