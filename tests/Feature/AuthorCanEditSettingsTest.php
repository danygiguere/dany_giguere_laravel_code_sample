<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AuthorCanEditSettingsTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $author;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'name' => 'johndoe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        $this->author = $this->actingAs($this->user);
    }

    /** @test */
    public function user_can_update_his_personal_info()
    {
        $response = $this->author->json('PUT', '/dashboard/update-personal-info/', collect($this->user)->toArray());
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.dashboard.personal-info.success-message')
            ]);
    }

    /** @test */
    public function user_can_update_his_password()
    {
        $user = New user;
        $user->current_password = 'secret';
        // the action will be browser tested
        $response = $this->author->json('PUT', '/dashboard/update-password/', collect($user)->toArray());
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => __('validation.dashboard.password.success-message')
            ]);
    }

}
