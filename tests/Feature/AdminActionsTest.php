<?php

namespace Tests\Feature;

use App\Draft;
use App\Jobs\SendPostRemovedEmail;
use App\Jobs\SendUserApplicationRejectedEmail;
use App\Jobs\SendUserBannedEmail;
use App\Jobs\SendUserReactivatedEmail;
use App\Jobs\SendUserSuspendedEmail;
use App\Post;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Support\Facades\Queue;

class AdminActionsTest extends TestCase
{
    use DatabaseTransactions;

    protected $superadmin;
    protected $user;
    protected $post;
    protected $draft;
    protected $postUrl;

    public function setUp()
    {
        parent::setUp();

        $this->superadmin = factory(User::class)->create([
            'role' => 'Superadmin',
        ]);
        $this->user = factory(User::class)->create([
            'role' => 'Applicant'
        ]);
        $this->draft = factory(Draft::class)->create([
            'user_id' => $this->user->id,
            'category_id' => 1,
        ]);
        $this->post = factory(Post::class)->create([
            'id' => $this->draft->id,
            'user_id' => $this->user->id,
            'category_id' => 1,
        ]);
        $this->postUrl = '/' . $this->post->category->url . '/' . $this->post->slug . '/' . $this->post->id;
    }

    /** @test */
    public function admin_can_reject_applicant()
    {
        Queue::fake();
        $this->actingAs($this->superadmin)->put("/users/".$this->user->url."/reject");
        $this->assertDatabaseHas('users', [
            'email' => $this->user->email,
            'role' => 'Applicant',
            'status' => 'Rejected'
        ]);
        Queue::assertPushed(SendUserApplicationRejectedEmail::class, 1);
    }

    /** @test */
    public function admin_can_ban_user()
    {
        Queue::fake();
        $this->actingAs($this->superadmin)->get("/users/".$this->user->url."/ban");
        $this->get('/users/'.$this->user->url)->assertStatus(404);
        Queue::assertPushed(SendUserBannedEmail::class, 1);
    }

    /** @test */
    public function admin_can_unban_user()
    {
        $this->actingAs($this->superadmin)->get("/users/".$this->user->url."/ban");
        Queue::fake();
        $this->actingAs($this->superadmin)->get("/users/".$this->user->url."/unban");
        $this->get('/users/'.$this->user->url)->assertStatus(200);
        Queue::assertPushed(SendUserReactivatedEmail::class, 1);
    }

    /** @test */
    public function admin_can_suspend_user()
    {
        Queue::fake();
        $this->actingAs($this->superadmin)->get("/users/".$this->user->url."/suspend");
        Queue::assertPushed(SendUserSuspendedEmail::class, 1);
    }

    /** @test */
    public function admin_can_unsuspend_user()
    {
        $this->actingAs($this->superadmin)->get("/users/".$this->user->url."/suspend");
        Queue::fake();
        $this->actingAs($this->superadmin)->get("/users/".$this->user->url."/unsuspend");
        Queue::assertPushed(SendUserReactivatedEmail::class, 1);
    }

    /** @test */
    public function admin_can_remove_post()
    {
        Queue::fake();
        $this->actingAs($this->superadmin)->json('PUT', '/posts/toggle-remove', ['post_id' => $this->post->id]);
        $this->get($this->postUrl)->assertStatus(404);
        Queue::assertPushed(SendPostRemovedEmail::class, 1);
    }

    /** @test */
    public function admin_can_reactivate_post()
    {
        $this->actingAs($this->superadmin)->json('PUT', '/posts/toggle-remove', ['post_id' => $this->post->id]);
        $this->actingAs($this->superadmin)->json('PUT', '/posts/toggle-remove', ['post_id' => $this->post->id]);
        $this->get($this->postUrl)->assertStatus(200);
    }

}
