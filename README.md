# README #

This project uses Laravel 5.5 and requires php 7.1 and Redis installed on the server.
Note that there needs to be at least one post (and one category) for the website to be functional.

### Installation ###
* `composer install`
* `cp .env.example .env`
* `php artisan key:generate`
* `npm install`
* `npm run dev` or `npm run watch-poll` or `npm run prod`


### Laravel Mix ###
- This website uses Laravel Mix to compile it's assets with Webpack
* To compile the assets, run `npm run dev` or `npm run watch-poll` or `npm run prod`.


### Migrations and Seeds ###
- This website uses migrations files to create/edit the db.
* To run the migrations, run `php artisan migrate`. In development, you can run `php artisan migrate:fresh` to rebuild the db or even `php artisan migrate:fresh --seed` to rebuild and seed it.


### S3
- All posts and users images are stored on S3, so you need to create a bucket on S3 and enter the credential in the .env
- You need to create a bucket for each env (dev, stg and prod), else you may overwrite production images.
- Note that some unit tests add images to the db (but they are removed right away after each test).


### Algolia Search ###
- This site uses scout and Algolia to perform intelligent searches
- You should create a different app for each env (dev, staging, prod) on Algolia.
* To import data to the Algolia db, run this command `php artisan scout:import "App\Post"`.
* Each saved and deleted posts post are automatically added and deleted from Algolia's indice.


### Redis Caching - Russian Doll caching implementation ###
- on the first request, db queries in Category.php fetch and cache 1) all categories, 2) all categories that have at least one post and order them by most popular.
- next, other db queries in Post.php fetch and cache id arrays for the most popular posts for all categories, for each category, the latest post for all categories, for each category and posts suggestions.
- for each post id a check is performed in the redis cache to see if the post_id key exists. If not, a db query fetches the post and then it is cached.
- when a user/admin create/edit/remove a post, the cache for that single post, for any related blade partials and for most DB queries are cleared. All other posts remained cached.
- db queries that perform algorythm are cached for shorter periods of time. Post are cached for longer periods of time. Post views seen on a post page are cached for a very short period of time (around 1 second).


### Laravel Horizon (Queues) ###
- This website uses Laravel Horizon to manages it's queues
* To start Horizon, log in as Superadmin and run `php artisan horizon` then go to `/horizon`.

Remember, queue workers are long-lived processes and store the booted application state in memory. As a result, they will not notice changes in your code base after they have been started. So, during your deployment process, be sure to restart your queue workers.
Therefore, if your updates involve changes in the workers processes, you need to put the application in maintenance and make sure there is no pending queues before deploying to production.


### VerificationController ###
- All routes that lead to the VerificationController require a verification_token, auth_token or session_key else the user will be redirected to a 403 (not authorized page)


### Unit testing ###
- This project uses php unit to unit test the application.
- First create a news_testing db and run: `php artisan migrate --database=mysql_testing`
- Copy/import the db from the dev env.
- To run a specific test, do : `phpunit tests/Unit/ExampleTest` or for the whole suite run: `phpunit`
- More info can be found here: `https://laravel.com/docs/5.5/http-tests`


### Acceptance testing (Dusk browser testing) ###
`php artisan dusk tests/Browser/RegisterTest` or `php artisan dusk`


### Creating a new Category ###
- As superadmin go to /dashboard/categories
- Once you create a category, you won't be able to see the category in the menu until someone adds a post to that category.


### Drafts and Posts model ###
- To create a post, you must create a draft and submit it.
- When you are create a draft, the info is saved automatically between 0 and 300ms after each changes.


### SignUp Process ###
- when a user signs up, a verify_token and a verify_token_created_at are created and a UserVerifyEmail is sent so the user.
- once the user clicks verify email (in the email), the email is verified and a UserSignedUpEmail is sent to the user and to the admin for approval.
- if the user tries to verify his email again, he is brought to the login page
- if the user tries to login without having verified, he will be offered to have a new UserVerifyEmail sent to him.
- Only admins or superadmins can make the applicant an author or reject the application.


### Policy Agreement ###


### @Todos ###
#### @todo in dev
- unit tests
- guideline
- queues priorities for dispatch(new SendUserSignedUpEmail($user)); and register..

#### @todo when in staging
- crons
- activate opCache
- https://github.com/spatie/laravel-analytics
- payments page
- logo

#### @todo later
- mysql backup to s3
- https://api.copyleaks.com/websitesapi/pricing
- advertisement page
- https://github.com/mewebstudio/Purifier ???
