<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDraftImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('draft_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('draft_id')->unsigned()->nullable();
            $table->foreign('draft_id')->references('id')->on('drafts')->onDelete('cascade');
            $table->boolean('copied_to_post')->default(false);
            $table->tinyInteger('image_number')->nullable();
            $table->string('filename')->nullable();
            $table->tinyInteger('order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('draft_images');
    }
}
