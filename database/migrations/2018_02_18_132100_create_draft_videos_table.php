<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDraftVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('draft_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('draft_id')->unsigned()->nullable();
            $table->foreign('draft_id')->references('id')->on('drafts')->onDelete('cascade');
            $table->string('type');
            $table->string('video_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('draft_videos');
    }
}
