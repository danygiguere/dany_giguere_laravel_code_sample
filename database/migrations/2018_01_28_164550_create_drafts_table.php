<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDraftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drafts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamp('first_published_on')->nullable();
            $table->boolean('unsaved_changes')->default(false);
            $table->integer('category_id')->default(1);
            $table->string('media_type')->default('image');
            $table->string('title')->nullable();
            $table->string('slug')->nullable()->unique();
            $table->text('text')->nullable();
            $table->tinyInteger('image_number_counter')->default(0);
            $table->string('image_numbers_to_be_deleted_from_post')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drafts');
    }
}
