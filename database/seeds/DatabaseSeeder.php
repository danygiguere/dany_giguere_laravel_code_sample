<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Generator as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $numberOfPosts = 50;

        $this->call(CategoriesTableSeeder::class);

        factory(App\User::class)->create([
            'name' => 'danygiguere',
            'real_name' => 'Dany Giguere',
            'public_name' => 'Dany',
            'url' => 'dany.giguere',
            'email' => 'danygiguere23@gmail.com',
            'password' => Hash::make('secret'),
            'auth_token' => createToken(),
            'role' => 'Superadmin',
            'status' => 'Active',
        ]);
        factory(App\User::class, 25)->create();

        // for each posts as post...
        for ($x = 1; $x <= $numberOfPosts; $x++) {
            // create views for each post
            for ($i = 0; $i <= 29; $i++) {
                factory(App\Views::class, 1)->create([
                    'post_id' => $x,
                    'created_at' => $faker->dateTimeBetween($startDate = "-$i day", $endDate = "-$i day", $timezone = date_default_timezone_get()),
                ]);
            }

            $user_id = rand(1, 10);
            $numberOfCategories = 8;
            $category_id = rand(1, $numberOfCategories);
            $media_type = 'image';
            $title = $faker->sentence($nbWords = 4, $variableNbWords = true);
            $slug = str_slug($title, '-');
            $text = $faker->paragraph($nbSentences = 3, $variableNbSentences = true);
            $created_at = $faker->dateTimeBetween($startDate = '-365 day', $endDate = 'now', $timezone = date_default_timezone_get());
            $updated_at = $created_at;
            factory(App\Post::class, 1)->create([
                'user_id' => $user_id,
                'category_id' => $category_id,
                'media_type' => $media_type,
                'title' => $title,
                'slug' => $slug,
                'text' => $text,
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ]);
            factory(App\Draft::class, 1)->create([
                'user_id' => $user_id,
                'first_published_on' => $created_at,
                'category_id' => $category_id,
                'media_type' => $media_type,
                'title' => $title,
                'slug' => $slug,
                'text' => $text,
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ]);
        }

        factory(App\PostImage::class, $numberOfPosts)->create([
            'post_id' => function () {
                static $index = 1;
                return $index++;
            },
            'image_number' => 0
        ]);
        factory(App\DraftImage::class, $numberOfPosts)->create([
            'draft_id' => function () {
                static $index = 1;
                return $index++;
            },
            'image_number' => 0
        ]);


////        FOR PRODUCTION LAUNCHING
//        factory(App\User::class, 1)->create([
//            'name' => 'danygiguere',
//            'real_name' => 'Dany Giguere',
//            'public_name' => 'Dany',
//            'url' => 'dany.giguere',
//            'email' => 'danygiguere23@gmail.com',
//            'password' => Hash::make('secret'),
//            'auth_token' => createToken(),
//            'role' => 'Superadmin',
//            'status' => 'Active',
//        ]);
//
//        factory(App\Category::class)->create([
//            'name' => 'Entertainment',
//            'url' => 'entertainment',
//        ]);
//
//        $user_id = 1;
//        $category_id = 1;
//        $media_type = 'image';
//        $title = $faker->sentence($nbWords = 4, $variableNbWords = true);
//        $slug = str_slug($title, '-');
//        $text = $faker->paragraph($nbSentences = 3, $variableNbSentences = true);
//        $created_at = $faker->dateTimeBetween($startDate = '-27 day', $endDate = 'now', $timezone = date_default_timezone_get());
//        $updated_at = $created_at;
//        factory(App\Post::class, 1)->create([
//            'user_id' => $user_id,
//            'category_id' => $category_id,
//            'media_type' => $media_type,
//            'title' => $title,
//            'slug' => $slug,
//            'text' => $text,
//            'created_at' => $created_at,
//            'updated_at' => $updated_at,
//        ]);
//        factory(App\Draft::class, 1)->create([
//            'user_id' => $user_id,
//            'first_published_on' => $created_at,
//            'category_id' => $category_id,
//            'media_type' => $media_type,
//            'title' => $title,
//            'slug' => $slug,
//            'text' => $text,
//            'created_at' => $created_at,
//            'updated_at' => $updated_at,
//        ]);
//        factory(App\PostImage::class, 1)->create([
//            'post_id' => function () {
//                static $index = 1;
//                return $index++;
//            },
//            'image_number' => 1
//        ]);
//        factory(App\DraftImage::class, 1)->create([
//            'draft_id' => function () {
//                static $index = 1;
//                return $index++;
//            },
//            'image_number' => 1
//        ]);
//        factory(App\Views::class, 1)->create([
//            'post_id' => 1,
//            'created_at' => today()->todatestring(),
//        ]);

        Cache::tags(['categories', 'posts'])->flush();

    }
}
