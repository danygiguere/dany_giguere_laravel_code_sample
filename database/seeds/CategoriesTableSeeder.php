<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('categories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // use factory to create users
        factory(App\Category::class)->create([
            'name' => 'Entertainment',
            'url' => 'entertainment',
        ]);
        factory(App\Category::class)->create([
            'name' => 'Opinions',
            'url' => 'opinions',
        ]);
        factory(App\Category::class)->create([
            'name' => 'Sport',
            'url' => 'sport',
        ]);
        factory(App\Category::class)->create([
            'name' => 'Politics',
            'url' => 'politics',
        ]);
        factory(App\Category::class)->create([
            'name' => 'Business',
            'url' => 'business',
        ]);
        factory(App\Category::class)->create([
            'name' => 'Arts',
            'url' => 'arts',
        ]);
        factory(App\Category::class)->create([
            'name' => 'Fashion',
            'url' => 'fashion',
        ]);
        factory(App\Category::class)->create([
            'name' => 'Technologies',
            'url' => 'technologies',
        ]);
    }
}
