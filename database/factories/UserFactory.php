<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;
    $realName = $faker->unique()->name;
    $name = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $realName));
    $email = $name . '@test.com';

    return [
        'name' => $name,
        'email' => $email,
        'password' => $password ?: $password = Hash::make('secret'),
        'remember_token' => str_random(10),
        'auth_token' => createToken(),
        'verification_token' => createToken(),
        'verification_token_created_at' => $faker->dateTimeBetween($startDate = '-101 day', $endDate = '-101 day', $timezone = date_default_timezone_get()),
        'last_agreed_policy' => '1',
        'role' => 'Author',
        'status' => 'Active',
        'suspended_count' => 0,
        'posts_removed_count' => 0,
        'real_name' => $realName,
        'public_name' => $realName,
        'url' => $name,
        'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'description' => '<p>' . $faker->paragraph($nbSentences = 3, $variableNbSentences = true) . '</p>',
        'public_email' => $faker->email,
        'public_email_2' => $faker->email,
        'website' => $faker->url,
        'website_2' => $faker->url,
        'website_3' => $faker->url,
        'facebook' => $faker->url,
        'twitter' => $faker->url,
        'google_plus' => $faker->url,
        'youtube' => $faker->url,
        'vimeo' => $faker->url,
        'instagram' => $faker->url,
        'image' => null,
        'created_at' => $faker->dateTimeBetween($startDate = '-100 day', $endDate = '-30 day', $timezone = date_default_timezone_get()),
        'updated_at' => $faker->dateTimeBetween($startDate = '-100 day', $endDate = '-30 day', $timezone = date_default_timezone_get()),
    ];
});
