<?php

use Faker\Generator as Faker;

$factory->define(App\Views::class, function (Faker $faker) {
    return [
        'post_id' => rand(1, 450),
        'total' => rand(1, 30000),
        'created_at' => $faker->dateTimeBetween($startDate = '-200 day', $endDate = 'now', $timezone = date_default_timezone_get()),
    ];
});
