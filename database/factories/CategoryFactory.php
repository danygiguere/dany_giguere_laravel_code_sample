<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name' => 'Entertainment',
        'url' => 'entertainment',
        'created_at' => $faker->dateTimeBetween($startDate = '-90 day', $endDate = 'now', $timezone = date_default_timezone_get()),
        'updated_at' => $faker->dateTimeBetween($startDate = '-90 day', $endDate = 'now', $timezone = date_default_timezone_get()),
    ];
});
