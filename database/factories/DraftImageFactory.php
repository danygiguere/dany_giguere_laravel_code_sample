<?php

use Faker\Generator as Faker;

$factory->define(App\DraftImage::class, function (Faker $faker) {
    static $index = 1;
    $filename = $faker->randomElement($array = array ('guy.jpg', 'elephant.jpg', 'mountain.jpg', 'vegetation.jpg'));
    return [
        'draft_id' => $index++,
        'filename' => $filename,
        'order' => 0,
        'created_at' => $faker->dateTimeBetween($startDate = '-60 day', $endDate = 'now', $timezone = date_default_timezone_get()),
        'updated_at' => $faker->dateTimeBetween($startDate = '-60 day', $endDate = 'now', $timezone = date_default_timezone_get()),
    ];
});
