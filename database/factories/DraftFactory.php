<?php

use Faker\Generator as Faker;

$factory->define(App\Draft::class, function (Faker $faker) {
    $title = $faker->sentence($nbWords = 4, $variableNbWords = true);
    return [
        'user_id' => rand(1, 10),
        'category_id' => rand(1, 8),
        'media_type' => 'image',
        'title' => $title,
        'slug' => str_slug($title, '-'),
        'text' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'created_at' => $faker->dateTimeBetween($startDate = '-365 day', $endDate = 'now', $timezone = date_default_timezone_get()),
        'updated_at' => $faker->dateTimeBetween($startDate = '-365 day', $endDate = 'now', $timezone = date_default_timezone_get()),
    ];
});
